# ===================================================================
#
# ZFS Storage REST API Sample
#
# - Overlay Analytics Datasets -
#
#  Expansion method - expand the specified column of the dataframe.
#
# Copyright 2019, Hisao Tsujimura
#
# ===================================================================

# -------------------------------------------------------------------------
# Libraries to Import
# -------------------------------------------------------------------------

# date/time support
from datetime import datetime as dt

# REST Support
import pprint
import requests
import urllib3

# System library
import sys

# Pandas
import pandas as pd
from pandas.io.json import json_normalize

# Matplotlib
# import matplotlib
import matplotlib.pyplot as plt


"""
This is a sample script to download two datasets from ZFSSA Analytics, and 
plot them in one graph.  It requires the below packages to make this script
run correctly.
    - pprint (for printing dictionaries and dataframes)
    - requests (communicating to ZFSSA via REST)
    - urllib3 (suppress SSL warning)
    - pandas (For manipulating data frames)
    - matplotlib (Plot finished datasets)
"""

# -------------------------------------------------------------------------
# Communication to ZFS Storage Using requests package
# -------------------------------------------------------------------------
# Connect to appliance and get a response.
def get_response(uri, username, password, proxy,
                 params={},
                 f_verify=False,
                 f_verbose=True,
                 f_show_response_code=True,
                 debug=False):
    """
    Connect to the URI and get response.
    :param uri: URL of the target host we are trying to connect such as
        http://<zfssa IP:215>/api/access/v1
    :param username: ZFS Storage User Name (Has to have access to datasets.)
    :param password: ZFS Storage password
    :param proxy: proxy
    :param params: other parameters to pass
    :param f_verify: verify flag for "requests" packages.  Default is False.
    :param f_verbose: extra message flag.  Default is True.
    :param f_show_response_code: Show HTTP response code.  Default True.
    :param debug: Show full URL that is passed to the target.  Default False
    :return: requests response structure.
    """
    if username == "":
        print("[ERROR] user ID is not provided.")
        sys.exit()
    if password == "":
        print("[ERROR] password is not provided.")

    if f_verbose == True:
        print("* Sending request to " + uri)
        if params != {}:
            print("* parameters = ", str(params))

    if params == {}:
        r = requests.get(uri, auth=(username, password), proxies=proxy,
                         verify=f_verify)
    else:
        r = requests.get(uri, auth=(username, password), proxies=proxy,
                         params=params, verify=f_verify)

    if debug:
        print("* (debug) URL =" + r.url)

    if f_show_response_code:
        print("HTTP Code: " + str(r.status_code))

    if r.status_code >= 400:
        sys.exit()

    return (r)


# Suppress SSL related warnings.
def suppress_warning():
    print("* NoteDisabling SSL warnings for demo purposes.")
    urllib3.disable_warnings()
    return


def get_dataset_names(target_host, username, password, proxies):
    """
    Given the target host name, use name, password and proxy, it will return
    a dictionary of dataset "name" and its href.
    :param target_host: ZFSSA host name
    :param username: user name
    :param password: password
    :param proxies: proxy
    :return: a dictionary of href
    TODO: This can be cached.
    """

    action="/api/analytics/v1/datasets"
    uri = target_host + action

    print("* Requesting dataset names available.")
    r = get_response(uri, username, password, proxies)

    ws_array = r.json()

    d = dict()
    for item in ws_array["datasets"]:
        key = item["name"]
        d[key] = item["href"]

    return(d)


def ds_pprint_dict(d):
    """
    Given the dictionary, show the name, href pair clean.
    :param d: dictionary that contains name and href
    :return: None.
    """

    if len(d) == 0:
        return

    print("{:30} {:35}".format("Name", "href"))
    for k in d:
        print("{:30} {:35}".format(k, d[k]))

    return

#----------------------------------------------------------------------------
# Row/Column Selection for Dataframes
#----------------------------------------------------------------------------


def select_columns(df, list_columns):
    """
    Given the a dataframe and a list of column names, choose them.
    :param df: data frame
    :param list_columns: column names in list format
    :return: data frame with the selected columns.
    """

    if len(list_columns) == 0:
        return df

    # make sure we don't miss out necessary columns.
    default_cols = ["startTime"]
    for item in default_cols:
        if item not in list_columns:
            list_columns.append(item)

    # now create a clean list of columns for selection.
    l = list()
    for item in list_columns:
        if item in df.columns:
            l.append(item)

    df_new = df[l]

    # return the new data frame with NaN handled as 0.
    return(df_new.fillna(0))


def drop_columns(df, list_columns):
    """
    Given a dataframe and a list of column names, drop them.
    :param df: data frame
    :param list_columns: column names in list format
    :return: data frames after dropping the columns.
    """

    if len(list_columns) == 0:
        return df

    for col in list_columns:
        if col in df.columns:
            df.drop(col, axis=1, inplace=True)

    return(df.fillna(0))


# ----------------------------------------------------------------------------
# Conversion Function
# ----------------------------------------------------------------------------

def expand_column(df, index="data.data", prefix="", drop_index=False):
    """
    Given the data frame and index of the data frame, this function will
    expand list of dictionaries in the field specified by "index," and
    add columns to a dataframe.
    :param df: Original dataframe
    :param index: index of the column we try to expand
    :param prefix: extra prefix to avoid redundant column name
    :param drop_index: drop the column specified by "index" before returning.
    :return: An expanded dataframe.
    """

    for item in df[index]:
        # field has to be a list, that contains the dictionary.
        if type(item) == list:
            if len(item) == 0:
                continue
            else:
                for e in item:
                    # decide the column name.
                    ops = e["key"]
                    if prefix == "":
                        key_prefix = ops + "_"
                    else:
                        key_prefix = prefix + "_" + ops + "_"

                    # Now add actual value to the dataframe
                    for statkey in e:
                        if statkey != "key":
                            column_name = key_prefix + statkey
                            # We don't have to worry about non-existent column
                            # name since Pandas takes care of it.if column name is not found, add.
                            df[column_name] = e[statkey]

    if drop_index:
        df.drop(index, axis=1, inplace=True)

    # make sure index and fillna is effective.
    df.set_index("startTime").fillna(0)
    return(df)



# ----------------------------------------------------------------------------
# For Debugging
# ----------------------------------------------------------------------------
def head_dict(d, n):

    print("=== dictionary content")
    if len(d) == 0:
        print("  Empty.")
    else:
        k = 0
        for i in d:
            if k <= n:
                print(str(i) + " | " + str(d[i]))
            k = k + 1

    return

# -------------------------------------------------------------------------
#  Visualization
# -------------------------------------------------------------------------

def my_draw_pd(df, title=""):
    """
    Given the data frame, draw a graph using matplot lib.

    :param df: dataframe
    :param title: title for the graph
    :return: None
    """

    if len(df) == 0:
        print("[Note] There is nothing to plot.")
        return
    else:
        if title == "":
            df.plot(grid=True, kind="line")
        else:
            df.plot(title=title, grid=True, kind="line")

    plt.show()

    return
# -------------------------------------------------------------------------
#  Main
# -------------------------------------------------------------------------

def main():

    # Variables we use
    proxies = {
        "http": None,
        "https": None,
    }

    # Data range to query
    params = {
        "startTime": "20190925T06:00:00",
        "span": "day",
        "granularity": "minute",
        "seconds": 86400
    }

    print("================================================")
    print(" Sample: Overlay ZFS Storage Analytics Datasets")
    print("================================================")

    print("Please enter user ID:")
    username = input()
    print("Please enter password:")
    password = input()

    print("[NOTE] Disabling SSL warnings for demo purposes.")
    urllib3.disable_warnings()
    print("[NOTE] Setting Panda's column witdh to 30.")
    pd.set_option('max_columns', 30)

    # Generate the full URL for API from the host name.
    # zfssa_hostname = "192.168.150.21"
    zfssa_hostname = "vcap-sca-00004.us.oracle.com"
    target_url = "https://" + zfssa_hostname + ":215"

    print("* Login")
    # Get first access
    action = "/api/access/v1"
    uri = target_url + action
    r = get_response(uri, username, password, proxy=proxies)

    print("* Get Dataset Names")
    ds_names = get_dataset_names(target_url, username, password, proxies=proxies)
    ds_pprint_dict(ds_names)

    print("* Query NFSv3 data from " + zfssa_hostname)
    action = ds_names["nfs3.ops[op]"] + "/data"
    uri = target_url + action
    r = get_response(uri, username, password, params=params, proxy=proxies)
    d = r.json()

    print("* JSON normalized data.")
    df = json_normalize(d, record_path="data").fillna(0)
    df.set_index("startTime")
    print("* Converted data.")
    df_nfs = expand_column(df, prefix="nfs3", drop_index=True)
    pprint.pprint(df_nfs)

    print("* Query Disk data from " + zfssa_hostname)
    action = ds_names["io.ops[disk]"] + "/data"
    uri = target_url + action
    r = get_response(uri, username, password, params=params, proxy=proxies)
    d = r.json()

    print("* JSON normalized data.")
    df = json_normalize(d, record_path="data").fillna(0)
    df.set_index("startTime")
    print("* Converted data.")
    df_sd = expand_column(df, prefix="sd", drop_index=True)
    # pprint.pprint(df_sd)

    print("* Merge Data Frames")
    df = pd.DataFrame.merge(df_nfs,df_sd,how="outer").fillna(0)
    #pprint.pprint(df.head)

    print("* Convert 'startTime' field to datetime")
    df["startTime"] = pd.to_datetime(df["startTime"], format="%Y%m%dT%H:%M:%S")

    pprint.pprint(df.head())

    print("* Draw a graph based on dataframe with Matplotlib")
    cols_to_select = [
        "nfs3_getattr_value",
        "nfs3_read_value",
        "sd_vcap-sca-00004/HDD 3_value",
        "sd_vcap-sca-00004/HDD 4_value"
    ]
    df = select_columns(df, cols_to_select)
    df.set_index("startTime", inplace=True)
    pprint.pprint(df.head())
    my_draw_pd(df, "Test Graph")

    print("### SAMPLE COMPLETES ###")
    return

if __name__=="__main__":
    main()
