#============================================================================
#
#  REST API Sample to Read Analytics Dataset (1)
#
# What This Does:
#   This sample sends the following REST requests and receives
#   the answers from the server on some analytics data
#
# Prerequisites:
#   - Python 3.6
#   - ZFS Storage Software 2013.06.05.7.17
#    (The above are tested versions.  The lower versions should work
#   as long as Python packages used here are available and ZFSSA
#   serves the same RESTful API.)
#
# Python Libraries:
#   If not installed in your environment, the following Python libraries
#   needs to be installed.
#       - pprint
#       - requests (to send and receive REST payload)
#       - urllib3 (to supress SSL warnings)
#       - pandas
#
# Caveat
#   For the sake of showing example, this script:
#       - intentionally ignores certificates.
# Copyright 2019 (c) Hisao Tsujimura
#
#============================================================================

# HTTP related Libraries

import pprint
import requests
import urllib3

# Pandas
from pandas.io.json import json_normalize
import json
import pandas as pd

#----------------------------------------------------------------------------
# Main
#----------------------------------------------------------------------------

""" 
This program does the followings.
1) Connect to ZFSSA via REST API
2) Retrieve Analytics dataset names
3) Retrieve Analytics data from the dataset.
You need to prepare the given dataset before start collecting data.
"""

def main():

    ##
    ## some global variables for connection.
    ##

    # Generate the full URL for API from the host name.
    zfssa_hostname = "192.168.150.21"
    target_url = "https://" + zfssa_hostname + ":215"

    # Some mess clean up such as warning and ignoring proxies
    print("* Note: Disabling SSL warnings for demo purposes.")
    urllib3.disable_warnings()

    noproxy = {
        "http": None,
        "https": None,
    }
    print("* Note: Proxy settings")
    print(noproxy)

    ##
    ## Ask for user ID and password
    ##

    print("Please enter user ID:")
    user_name = input()
    print("Please enter password:")
    password = input()

    ##
    ## Log in using REST
    ##

    action = "/api/access/v1"
    uri = target_url + action

    print("* Login")
    r = requests.get(uri,
        auth=(user_name, password),
        proxies=noproxy, verify=False)

    if r.status_code != 200:
        print("HTTP " + str(r.status_code))

    #pprint.pprint(r.json())

    ##
    ## Get Dataset Names
    ##

    action = "/api/analytics/v1/datasets"
    uri = target_url + action
    print("* Get Datset Names")

    r = requests.get(uri,
        auth=(user_name, password),
        proxies=noproxy, verify=False)

    if r.status_code != 200:
        print("HTTP " + str(r.status_code))

    #pprint.pprint(r.json())

    ##
    ## Get Actual Data from NFSv3 ops.
    ##

    action = "/api/analytics/v1/datasets/nfs3.ops[op]/data"
    uri = target_url + action

    print("* Query NFSv3 ops data")

    r = requests.get(uri,
        auth=(user_name, password),
        proxies=noproxy, verify=False)

    if r.status_code != 200:
        print("HTTP " + str(r.status_code))

    pprint.pprint(r.json())

    ##
    ## Get Actual Data for NFSv3 Ops with Date/Time
    ##

    params = {
        "startTime" : "20190808T00:00:00",
        "span" : "day",
        "granularity" : "hour"
    }

    print("* Query NFSv3 ops data with startTime and granularity")
    r = requests.get(uri,
        auth=(user_name, password),
        proxies=noproxy, params=params, verify=False)

    if r.status_code != 200:
        print("HTTP " + str(r.status_code))

    r_json = r.json()
    pprint.pprint(r_json)

    print("# End of Sample")

if __name__ == "__main__":
    main()
