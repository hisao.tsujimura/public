# README.md (EN)

## About Scripts

### zfs_freq_events.sh

This script takes the output of zpool history -il and makes frequency counts in the specified resolution.  The duration can be 10 seconds, 1 minute 10 minutes or an hour.


Counted Events： 
    Create Snapshot Rename Destroy Hold Release replay_inc_sync replay_full_sync

#### Usage

The script was tested on Solaris 11 update 1 platform.
The standard unix string related commands are used, therefore,
if the same style input is give, I believe it will work on other flavors of Unix.

1) Download the file and give execute permission.

2) The below is the command syntax.  If the second parameter is omitted, it will default as 10 minutes duration to count events.

  zfs_freq_events.sh <path to input file> [10s|10sec|1m|1min|10m|10min|1h]

3) The below is a sample run log.

```
-bash-4.1$ ~/zfs_freq_events.sh ./POOL2.history 1h
zfe: zfs_freq_events.sh making a filtered file...
zfe: version=1.3

Time (per 1h)        Create Snapshot Rename Destroy  Hold Release Inc_sync Full_sync
2016-02-20.16:00-59       0       20     33     133     0       0       44        2
2016-02-20.17:00-59       0      137     33      81     0       0       13        2
2016-02-20.18:00-59       0      111     33     104     0       0       16        0
2016-02-20.19:00-59       0      112     33     104     0       0       15        0
2016-02-20.20:00-59       0      112     33      80     0       0       39        0
2016-02-20.21:00-59       0      362    190     240     0       0       40        0
```
#### Event Description

|Event Type      |Description                                 |
|----------------|--------------------------------------------|
|Create          |Number of dataset creation                  | 
|Snapshot        |Number of snapshot creation                 |
|Destroy         |Number of dataset deletion                  |
|Hold            |Number of hold events by other txg          |
|Release         |Number of hold event releases               |
|Inc_sync        |Number of incremental ZIL replays           |
|Full_sync       |Number of full ZIL replays                  |

In ZFSSA, we make snapshots when doing replication or NDMP backup.
Destroy includes both file system and snapsot destroys.

### zfs_summarize_events.sh

This script takes the output of zpool history -il in a file and summarizes the events by dataset ID.
Since this script uses the dataset ID as a key to an assosicative array, awk symbol table may overflow when the input file is too large.

I recommend reducing the size to minimum you need before feeding the file to this script such as by dataset ID or by events.

Events: Create Snapshot Rename Destroy
        Start/stop of shadow migration

__Known Issue__

This script does not care about the multiple events for the same dataset and records the last time stamp of the event.  For example, if the multiple snapshots are taken against the same dataset, we will see the time stamp of the last snapshot in the report.


#### Usage

The script was tested on Solaris 11 platform.
The standard unix string related commands are used, therefore,
if the same style input is give, I believe it will work on other flavors of Unix.

1) Download the file and give execute permission.

```
chmod +x ./zfs_summarize_event.sh
```

2) When you run it, specify the file name you want to summarize.

```
./zfs_summarize_event.sh ./pool0.history
```

3) The below is a sample output.

```
"Dataset","Created","Snapshot","Rename","Destroy","Shadow(Start)","Shadow(End)","Path"
2240,,,,2014-11-15.07:47:52,,,""
2268,,,,2014-11-15.10:47:41,,,""
36887,,2014-11-15.10:03:33,,,,,""
36932,,2014-11-15.10:03:33,,,,,""
37033,,2014-11-15.10:03:33,,,,,""
37080,,2014-11-15.10:03:33,,,,,""
37123,,2014-11-15.10:03:33,,,,,""
37168,,2014-11-15.10:03:33,,,,,""
37264,,2014-11-15.10:03:33,,,,,""
(.....)
573913,,,,,2014-12-02.09:45:02,2014-12-02.09:47:20,"shadow=file:///export/projeca/sharea"
573919,,,,,2014-12-02.09:47:18,2014-12-02.09:48:54,"shadow=file:///export/projectb/shareb"
573925,,,,,2014-12-02.09:48:56,2014-12-02.09:56:50,"shadow=file:///export/projectc/sharec"
573931,,,,,2014-12-02.09:54:27,2014-12-02.12:46:48,"shadow=file:///export/projectd/shared"
```

|Field|Description|
|----------|---|
|Dataset|Dataset ID|
|Created|Date/time of dataset creation|
|Snapshot|Date/time of a snapshot|
|Rename|Date/time of dataset rename|
|Destroy|Date/time of dataset destroy|
|Shadow(Start)|Date/time of setting up a shadow migration|
|Shadow(End)|Date/time of shadow migration completion|
|Path|Source path for shadow migration|

__end of README-en.md__
