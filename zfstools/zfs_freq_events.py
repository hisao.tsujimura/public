#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
# --------------------------------------------------------------
#
# ZFS Tools
#
# Script name: zfs_freq_events.py
#
# How To Run:
#   zfs_freq_events.py [-i|--i <filename for analysis>]
#                      [-d|--duration [1m|10m|1h]
#
# What This Does:
#   This is a Python version of zfs_freq_events.sh.
#   This reads the pool zpool -il <pool name> output and
#   counts the number of events by event types.
#
# Copyright 2017, 2019, by Hisao Tsujimura
#
#--------------------------------------------------------------

import argparse
import os, sys
from os import path
from enum import Enum

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------


# Normalize Path
def normalize_path(infile):
    if infile == None:
        return("")
    path_name = path.abspath(path.expanduser(infile))
    return(path_name)


# Open file

# Process arguments
def get_opts():

    parser = argparse.ArgumentParser(prog="zfs_freq_events.py", description="ZFS Event Frequency Report")
    parser.add_argument("-i", "--infile", dest='infile', required=True, help="Path to ZFS Pool History File")
    parser.add_argument("-d", "--duration", dest='duration', choices=['1m', '10m', '1h'], help="Statistics duration (1m/10m/1h")

    return(parser.parse_args())

#--------------------------------------------------------------
# Run if this is main
# --------------------------------------------------------------

if __name__ == "__main__":
    param = get_opts()

    mypath = normalize_path(param.infile)
    print("Input = " + param.infile)
    print("(Normalized = " + mypath)

    sys.exit()


# End of script
