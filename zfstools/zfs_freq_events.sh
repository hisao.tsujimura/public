#!/bin/ksh
#==============================================================================
#
# script name: zfs_freq_events
#
# What this does:
#
# This script takes the output from zpool history -il and
# count the frequency of events.
# The resolution of events is 10 seconds, 1 minutes, 10 minutes and 1 hour.
# The default value is 10 minutes.
#
# Usage:
#
# zfs_freq_events.sh <history log name> <resolution in 10sec, 1min, or 10 min 1h>
#
# Example:
#
# zfs_freq_events.sh mypool.history 10sec
#
# Limitation:
#  In order to limit the type of events, this script only takes the following
#  events.
#       create, snapshot, rename, destroy
#
#* Revision History
# 1.0 - 2015.01.26 - Hisao Tsujimura
# 1.1 - 2015.01.27 - Hisao Tsujimura, added 1h resolution.
# hotfix 20150128a - Hisao Tsujimura, fixing typos.
# hotfix 20150129a - Hisao Tsujimura, fixing typos.
# 1.2 - 2015.03.03 - Hisao Tsujimura, added hold/release and inc_syc
# hotfix 20150303a - Hisao Tsujimura, fixed event type for hold/release events
# 1.3 - 2016.08.26 - Hisao Tsujimura, adding replay_full_sync event
#==============================================================================
# variables
#==============================================================================
VERSION='1.3' # version number
PGMID='zfe' # program ID

TEMPDIR=`pwd`/tempdir
TEMPFILE=$TEMPDIR'/'$PGMID'.tmp'
TEMPFILE2=$TEMPDIR'/'$PGMID'.tmp2'

mkdir -p $TEMPDIR

#==============================================================================
# main
#==============================================================================
#  Change the scope by modifying the egrep clause below.
echo $PGMID': zfs_freq_events.sh making a filtered file...'
echo $PGMID': version='$VERSION
echo ''
cat $1 | egrep 'create|rename|destroy|snap|release|hold|inc_sync|full_sync' \
    > $TEMPFILE

RES=$2

## check resolution.  if invalid parameter is set, the default is 10 minutes.
case $2 in
   '10sec'|'10s')
      RES='10sec';;
   '1min'|'1m')
      RES='1min';;
   '10min'|'10m')
      RES='10min';;
    '1hour'|'1h')
      RES='1h';;
    *)
      RES='10min';;
esac

### process the time stamp accordingly.
### the time stamp in the original file comes in the format below.
###     2015-01-23.18:13:27
### So for 10 second resolution, we replace he last digit to 0.
### For 1 minute resolution,  chop off the last 3 letters including a colon.
### For 10 minute resolution, we chop off the last 4 letters, then add 0.
### For 1 hour resolution, we chop off the last 6 letters including two colons.

case $RES in
'10sec')
    cat $TEMPFILE | awk '{$1=sprintf("%s0",substr($1,1,length($1)-1)); print $0}' > $TEMPFILE2
    ;;
'1min')
    cat $TEMPFILE | awk '{$1=sprintf("%s",substr($1,1,length($1)-3)); print $0}' > $TEMPFILE2
    ;;
'10min')
    cat $TEMPFILE | awk '{$1=sprintf("%s0",substr($1,1,length($1)-4)); print $0}' > $TEMPFILE2
    ;;
'1h')
    cat $TEMPFILE | awk '{$1=sprintf("%s:00-59",substr($1,1,length($1)-6)); print $0}' > $TEMPFILE2
    ;;
 esac

### We assume $3 has ZFS event name.  This is actually slightly different from
### one event to another.  For now, the following events has event name in $3.
### create|rename|destroy|snapshot

cat $TEMPFILE2 | \
nawk 'BEGIN{
    # initialize min and max values for dataset nubmers.

    p_time="XXXX";   # set previous time
    event[1]=0;     #number of create events
    event[2]=0;     #number of snapshot events
    event[3]=0;     #number of renae events 
    event[4]=0;     #number of destroy events
    event[5]=0;     #number of hold txg events
    event[6]=0;     #number of release txg events
    event[7]=0;     #number of replay_inc_syc events
    event[8]=0;     #number of replay_full_syc events

    f1=sprintf("%19s ","Time (per '$RES')");
    printf("%s Create Snapshot Rename Destroy  Hold Release Inc_sync Full_sync\n",f1);
}

{
    ### How this awk works
    ###
    ### Assuming that the input is sorted alphabetially,
    ### when the time changes the $1 of the input is different from $1 of the
    ### previous line.  We temporarily set XXXX in the p_time for comparison and start.
    ###
    ### When we have the line with different string, we print out the accumulated number
    ### of events, clear the array and set the p_time to $1.  If the p_time is XXXX,
    ### we know we have not collected data, so we just set p_time to a new value.

    ### Check and print the content
    if ($1 != p_time)
        {
            if (p_time !~ /XXXX/ )
            {
                printf("%19s %7d %8d %6d %7d %5d %7d %8d %9d\n",p_time,
                    event[1],event[2],event[3],event[4],
                    event[5],event[6],event[7],event[8]);
                event[1]=0;
                event[2]=0;
                event[3]=0;
                event[4]=0;
                event[5]=0;     #number of hold txg events
                event[6]=0;     #number of release txg events
                event[7]=0;     #number of release inc_syc events
                event[8]=0;     #number of release full_syc events
            }
            p_time=$1;
        }

    ## common item to save
    e_type=$3;  # event type.

    ## convert event type string to number.

    if (e_type ~ /create/ )  e_type_no=1;
    if (e_type ~ /snap/ )    e_type_no=2;
    if (e_type ~ /rename/ )  e_type_no=3;
    if (e_type ~ /destroy/ ) e_type_no=4;

    ## for hold and release, the e_type = user
    if (e_type ~ /user/ ) 
     {
         if ( $4 ~/hold/    ) e_type_no=5;
         if ( $4 ~/release/ ) e_type_no=6;
     }

     if ( e_type ~ /replay_inc_sync/ ) e_type_no=7;
     if ( e_type ~ /replay_full_sync/ ) e_type_no=8;


    #count up on event.
    event[e_type_no]=event[e_type_no]+1;

}
END{
    ## Make sure we print the last one

    ##printf("### DEBUG END Clause\n");

                printf("%19s %7d %8d %6d %7d %5d %7d %8d %9d\n",p_time,
                    event[1],event[2],event[3],event[4],
                    event[5],event[6],event[7],event[8]);
}'

## Cleaning up
## echo 'Deleting the work directory.'
rm -rf $TEMPDIR 2>/dev/null
echo '#### END OF REPORT ###'

### END OF SCRIPT ###
