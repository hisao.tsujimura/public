#!/bin/ksh
#==============================================================================
#
# script name: zfs_summarize_events.sh
#
# What this does:
# 
# This script takes the output from zpool history -il and 
# makes summary per data set.
#
# Usage:
#
# zfs_summarize_events.sh <history log name>
#
# Caveat:
#   The script only takes the time stamp of the last entry for the event
#   in the log.  Therefore the log has to be sorted by the time stamp.
#   (which is usually the case for zpool history -il command.)
#
#    Due to the limited number of symbols that nawk can use,
#    it could not process the history files with too many lines.
#    It would worth reducing the number of lines by changing the egrep phrase
#    before nawk.  Otherwise the symbol table overflows.
#
# Author:
#  Hisao Tsujimura (hisao.tsujimura@oracle.com) 2014-11-19
#  Hisao Tsujimura (hisao.tsujimura@oracle.com) 2014-12-03
#   - added shadow migration summary
#
#==============================================================================
# variables
#==============================================================================
VERSION=1.1 # version number
PGMID='zse' # program ID

#==============================================================================
# main
#==============================================================================
#  Change the scope by odifying the egrep clause below.
cat $1 | egrep 'create|rename|destroy|snap|shadow' | \
nawk 'BEGIN{
    # initialize min and max values for dataset nubmers.
    ds_max=0;
    ds_min=99999999;
    printf("\"Dataset\",\"Created\",\"Snapshot\",\"Rename\",\"Destroy\",\"Shadow(Start)\",\"Shadow(End)\",\"Path\"\n");
}

{
    ## common item to save 
    e_type=$3;  # event type.
    e_dsno=$7;  # dataset number.
    e_time=$1;  # time stamp
    
    ## convert event type string to number.

    if (e_type ~ /create/ )  e_type_no=1;
    if (e_type ~ /snap/ )    e_type_no=2;
    if (e_type ~ /rename/ )  e_type_no=3;
    if (e_type ~ /destroy/ ) e_type_no=4;

    ### Above are fine, but shadow migration history has unique 
    ### format, so overriding some values here.
    if ( $0 ~ /shadow=file:/)
    {
        e_type_no=5;    # a mount point for shadow migraiton 
        e_dsno=$9;
        events[7,e_dsno]=$6;    # path
    }
    if ( $0 ~ /shadow=none/ )
    {
        e_type_no=6;    # shadow migration complete
        e_dsno=$9;
    }

    ## take time stamp and store in array.
    ## if there is the same event for the same dataset, 
    ## the last event will be kept since we don not check
    ## anything here.

    events[e_type_no,e_dsno]=e_time;
    events[0,e_dsno]=e_dsno; #dataset number set to check if there is data.

    ###
    ### printf("### DEBUG ds=%d, flag=%s\n",e_dsno,events[0,e_dsno]);

    ## check if the dataset number is larger than before.  
    if (ds_max <= e_dsno ) ds_max=e_dsno;
    ## check if the dataset number is smaller than minimum.  
    if (ds_min >= e_dsno ) ds_min=e_dsno;

}
END{
    ### writing out the array
    ###printf("### DEBUG - END clause.\n");

    recno=ds_min;   # start from smallest record number 

    while (recno <= ds_max)
    {
        ##printf("### DEBUG recno=%d, flag=%s\n",recno,events[0,recno]);
        ## if there is no event for the given recno,
        ## the content of events[0,recno] will be undefined.
        ## This happened to be judged by >=1 in this version of nawk.
        ## It is not a clean if statement.
        
        if ( events[0,recno] >= 1)
        {
            printf("%s,",events[0,recno]);
            printf("%s,",events[1,recno]);
            printf("%s,",events[2,recno]);
            printf("%s,",events[3,recno]);
            printf("%s,",events[4,recno]);
            printf("%s,",events[5,recno]);
            printf("%s,",events[6,recno]);
            printf("\"%s\"\n",events[7,recno]);
        }
        recno=recno+1;
    }
}'
