#!/usr/sbin/dtrace -Cs
/*
 * ----------------------------------------------------------
 * 
 * script name: spa_export.d
 * 
 * what this does: 
 *   This script tries to measure time spent by each ZFS
 *   functions after zpool export calls spa_export. 
 *
 * revisions:
 *  2015-12-23 Hisao Tsuijmura
 *
 * -----------------------------------------------------------
 */

#pragma D option quiet

#define TIMESTAMP   printf("%Y %u ", walltimestamp, timestamp);

#define TIMESTAMP_PROBE printf("%Y %u %s ", walltimestamp, timestamp, \
            probename);

#define TIMESTAMP_FUNC  printf("%Y %u %s ", walltimestamp, timestamp, \
            probefunc);

#define TIMESTAMP_ALL  printf("%Y %u %s %s ", walltimestamp, timestamp, \
            probefunc, probename);


/* -----------------------------------------------------------
 *  main code here 
 * -----------------------------------------------------------*/

BEGIN{
    /* show start message with time stamp */
    TIMESTAMP_ALL
    printf("tracing started.\n");

    /* initialize flags */
    self->f_exporting=0;
}

END{
    /* show complete message with time stamp */
    TIMESTAMP_ALL
    printf("tracing completed.\n");

    /* initialize flags */
    self->f_exporting=0;
}

/*
 * We are interested in measuring time after spa_export is called.
 * So, we set the flag when entry probe is called and reset the flag
 * when return probe is called.
 *
 * You could add pool name as a condition to filter out the events,
 * but here we take anything for the sake of simplity for now.
 */

fbt:zfs:spa_export:entry
{
    TIMESTAMP_ALL
    printf(" - export started.\n");

    /* set the flag to indicate start priting */
    self->f_exporting=1;
}

fbt:zfs:spa_export:return
{
    TIMESTAMP_ALL
    printf(" - export completed.\n");

    /* make sure we reset the flag */
    self->f_exporting=0;

    /* also, get out of dtrace when this is called */
    exit (0);
}

/*
 * We are interested in any calls after spa_export is called.
 * However, since the number of entries will be very large, 
 * we take only functions within ZFS, which are related to 
 * SPA (space allocator) and vdev.
 */
fbt:zfs:spa_*:entry,
fbt:zfs:vdev_*:entry 
/self->f_exporting == 1/
{
    TIMESTAMP_ALL;
    printf("\n");

    /* set the start time in self->s */
    self->s = timestamp;
}

fbt:zfs:spa_*:return,
fbt:zfs:vdev_*:return 
/self->f_exporting == 1/
{
    TIMESTAMP_ALL;

    /* calculate the time.  the time is in micro sec. */
    time_spent=self->s - timestamp;
    printf(" (elapsed %u usec)\n",time_spent);

    /* set the start time in self->s */
    self->s = timestamp;
}

