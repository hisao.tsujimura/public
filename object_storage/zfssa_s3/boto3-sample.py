#
# Sample Script to Access ZFSSA S3-Compatible Share
#
# This is a sample script for accessing ZFSSA share with S3-Compatible Protocol
# It requires boto3 package from Amazon Web Services.
# The script is provided "as is," and use it at your own risk.
#
#
# Copyright 2022, by Hisao Tsujimura
#

import boto3
from botocore.config import Config
import urllib3
import pprint

def main():
    """
    Sample code for S3 access on ZFSSA
    :return: None
    """

    # It is not a good idea to hard code your access key and secret key.
    # Use other means such as environment variables to protect your keys.

    access_key = 'mys3user_key'    # enter your access key name, not user name.
    secret_key = '4723ce30ee623d971a6b0d3a6411560a44a0d6521dcec0420a4f167cb5777119'

    # ZFS Storage - Hostname and Share Name
    node_name = "vcap-sca-00002.us.oracle.com"
    port = 443  # specify the port number since we are using HTTPS
    port_str = ":" + str(port)
    service_name = "/s3/v1"
    share_name = "/export/mys3"

    base_url = node_name + port_str + service_name
    end_point_url = "https://" + base_url + share_name
    print("# End Point URL")
    print(end_point_url)

    # Suppress Warnings
    urllib3.disable_warnings()

    # boto Configuration to avoid use of proxy.
    proxy_definitions = {
        'http': '',
        'https': ''
    }

    my_config = Config(
        signature_version='v4',
        proxies=proxy_definitions
    )

    #
    # Actual Work
    #
    print("# Making a session for ZFS Storage...")
    b3_session = boto3.Session(aws_access_key_id=access_key,
                               aws_secret_access_key=secret_key,
                               region_name='ashburn-us')

    print("# Connecting a session to ZFS Storage...")
    b3_client = b3_session.client('s3', endpoint_url=end_point_url, verify=False,
                                  config=my_config)

    print("# Making a request to ZFS Storage...")
    bucket = b3_client.create_bucket(Bucket="newbucket")

    print("DONE! (Creating a bucket)")
    print(type(bucket))
    pprint.pprint(bucket)

    bucket_dict = b3_client.list_buckets()
    bucket_list = bucket_dict["Buckets"]
    for bucket in bucket_list:
        print("Name " + bucket["Name"] + " Creation Date:" + str(bucket["CreationDate"]))

    return

if __name__ == "__main__":
    main()

#
# References:
# "Boto and Boto3," Oracle® ZFS Storage Appliance Object API Guide for Amazon S3 Service Support, Release OS8.8.x
# <https://docs.oracle.com/en/storage/zfs-storage/zfs-appliance/os8-8-x/amazon-s3-api-guide/gsyqi.html>
#
# "Object API Configuration," Oracle® ZFS Storage Appliance Administration Guide, Release OS8.8.x
# <https://docs.oracle.com/en/storage/zfs-storage/zfs-appliance/os8-8-x/admin-guide/grdyg.html#scrolltoc>
#
# Disable SSL Warning (not recommended)
# https://urllib3.readthedocs.io/en/1.26.x/advanced-usage.html#ssl-warnings
#
# (END OF CODE)