# README.md

## iostat2csv.py Script

`iostat2csv.py` script converts `iostat` data into a CSV file.

### Platform

Solaris 11.4

### Input Data

#### Encoding

We assume it is `us_US.UTF-8` encoding.

#### Format
We assume the input data comes in the following format,
as per Solaris extended iostat output.

The time stamp is 1 minute apart and there are 13 data points.
(5 seconds apart, starting with 0 second.)

```text
/usr/bin/iostat -x 5 13


Mon Mar  4 23:32:23 UTC 2024 - started
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
blkdev0   1.4   46.6   99.5 1061.2  0.0  0.0    0.0    0.1   0   0
blkdev1   1.4   48.3   98.9 1061.3  0.0  0.0    0.0    0.1   0   0
sd0      50.4   23.8 6116.8 4635.0  0.0  0.7    0.0    9.8   0  30
sd1      50.4   23.8 6115.5 4634.9  0.0  0.7    0.0    9.9   0  30
(......)
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
blkdev0  11.4   58.5  602.9 1197.0  0.0  0.0    0.0    0.1   0   0
blkdev1  14.4   64.3  688.3 1197.0  0.0  0.0    0.0    0.1   0   0
(......)
Mon Mar  4 23:33:23 UTC 2024 - ended


Mon Mar  4 23:35:44 UTC 2024 - started
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
```

### Expected Output

It generates a CSV file to yeild a table like below.  
The '%' signs are not suitable for column names, therefore, the program replces  
it with 'pct_'.  The column names are hard-coded.  
The time is calclated from the beginning of the block ('- started' line) and  
added to the first column.

| Time                 | device  | r/s  | w/s  | kr/s     | kw/s   | wait | actv | wsvc_t  | asvc_t | %w | %b |
|----------------------|---------|------|------|----------|--------|------|------|---------|--------|----|----|
| 2024-03-04T23:32:23Z | blkdev0 | 1.4  | 46.6 | 99.5     | 1061.2 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:23Z | blkdev1 | 1.4  | 48.3 | 98.9     | 1061.3 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:24Z | blkdev0 | 11.4 | 58.5 | 602.9    | 1197.0 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:24Z | blkdev0 | 11.4 | 58.5 | 602.9    | 1197.0 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:35:44Z | sd0     | 50.4 | 23.8 | 6116.8 | 4635.0 | 0.0 | 0.7  | 0.0 | 9.8 | 0 | 30 |

## iostat2csv-linux.py Script

This is a Python script that looks into each vmstat data whose name ends with '.dat,"
covert it into CSV and writes to a specified output file.
Currently, the input data directory and output data file is hard coded.

## Supported Platform

Oracle Enterprise Linux 8, but parsing of data can be run on any platform that runs Python and
understands the Unix path correctly.

### Input Data

#### Encoding

We assume it is `us_US.UTF-8` encoding.

#### Format
We assume the input data comes in the following format,
as per Solaris extended iostat output.

It assumes that each block of data starts with zzz.
We consider the rows between "Device" line and "zzz" line to be parsed.
The header for CSV file also is hardcoded for analysis tools to read.

```text
zzz ***Mon Oct 7 17:00:32 JST 2024
avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.49    0.00   18.03    1.64   10.82   67.02

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
sdb               0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
sda               0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
dm-0              0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
sdc               0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
sdd               0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.00
sde               0.00     0.00 2163.00  470.00 42512.00  1202.00    33.20    18.41    6.98    8.26    1.11   0.38 100.00
```



### Expected Output

It generates a CSV file to yeild a table like below.  

```
|time|device|rrqm_per_sec|wrqm per_sec|r_s|w_s|rkB_s|wkB_s|avgrq-sz|avgqu-sz|await|r_await|w_await|svctm|pct_util|
|----|------|------------|------------|-------|-----|-----|--------|--------|-----|-------|-------|-----|--------|
2024-10-08T10:00:03|sdb |0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|0.00|
2024-10-08T10:00:03|sda |0.00|3.00|0.00|58.00|0.00|6636.00|228.83| 0.02|0.33|0.00|0.33|0.03|0.20|
```
(END OF README.md)
