# README.md

## このスクリプトについて

`iostat2csv.py` は Solaris の `iostat` のデータを CSV に変換します。

### プラットフォーム

Solaris 11.4

### 入力データの要件

#### エンコーディング

us_US.UTF-8 を想定しています。

#### フォーマット

Solaris の iostat の形式（以下に示す物）を想定しています。

タイムスタンプが 1分毎に刻まれ、それぞれに 13 のデータポイントがあるものとしています。
（5秒毎、0秒からのスタート）

```text
/usr/bin/iostat -x 5 13


Mon Mar  4 23:32:23 UTC 2024 - started
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
blkdev0   1.4   46.6   99.5 1061.2  0.0  0.0    0.0    0.1   0   0
blkdev1   1.4   48.3   98.9 1061.3  0.0  0.0    0.0    0.1   0   0
sd0      50.4   23.8 6116.8 4635.0  0.0  0.7    0.0    9.8   0  30
sd1      50.4   23.8 6115.5 4634.9  0.0  0.7    0.0    9.9   0  30
(......)
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
blkdev0  11.4   58.5  602.9 1197.0  0.0  0.0    0.0    0.1   0   0
blkdev1  14.4   64.3  688.3 1197.0  0.0  0.0    0.0    0.1   0   0
(......)
Mon Mar  4 23:33:23 UTC 2024 - ended


Mon Mar  4 23:35:44 UTC 2024 - started
                     extended device statistics
device     r/s    w/s   kr/s   kw/s wait actv wsvc_t asvc_t  %w  %b
```

### Expected Output

以下のような表を作成するためにの CSVを出力します。
なお、列名として取り回しが良くないので、% は pct_ と置き換えています。
各ブロック (- started から）の経過時間を計算し、各行に追加しています。

| Time                 | device  | r/s  | w/s  | kr/s     | kw/s   | wait | actv | wsvc_t  | asvc_t | %w | %b |
|----------------------|---------|------|------|----------|--------|------|------|---------|--------|----|----|
| 2024-03-04T23:32:23Z | blkdev0 | 1.4  | 46.6 | 99.5     | 1061.2 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:23Z | blkdev1 | 1.4  | 48.3 | 98.9     | 1061.3 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:24Z | blkdev0 | 11.4 | 58.5 | 602.9    | 1197.0 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:32:24Z | blkdev0 | 11.4 | 58.5 | 602.9    | 1197.0 | 0.0 | 0.0  | 0.0 | 0.1    | 0 | 0  |
| 2024-03-04T23:35:44Z | sd0     | 50.4 | 23.8 | 6116.8 | 4635.0 | 0.0 | 0.7  | 0.0 | 9.8 | 0 | 30 |

(End of Memo)
