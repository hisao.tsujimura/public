# =========================================================================
#
#   Analysis Script
#
#   Script name: iostat2csv.py
#
#   What it does:
#       This script converts Solaris "extended" iostat data into a CSV
#       file.
#
#   Copyright 2024, by Hisao Tsujimura
#
# =========================================================================
"""
See README.md for assumed format and sample output.
"""
from os import path
import datetime
import locale


def csv_write(data:list, quote=False) -> str:
    """
    Given the list of data, write out a row in CSV format.
    When the type of column can be considered as number, write without quotes.
    If the column is string, surround it by double quotes.
    Also, if the type of the field is datetime, then convert it int ISO8601
    format.
    :param data: string
    :return: CSV formatted string
    """

    csv_string = ""
    counter = 0
    for item in data:
        item_is_number = False
        if type(item) is not datetime.datetime:
            try:
                _ = int(item)
                item_is_number = True
                csv_string = csv_string + item
            except:
                try:
                    _ = float(item)
                    item_is_number = True
                    csv_string = csv_string + item
                except:
                    if item_is_number:
                        csv_string = csv_string + item
                    elif type(item) is str and quote:
                        csv_string = csv_string + "\"" + item + "\""
                    elif type(item) is str and quote is False:
                        csv_string = csv_string + item
        else:
            # convert timestamp to ISO8601
            csv_string = csv_string + item.isoformat()

        counter = counter + 1
        if counter < len(data):
            csv_string = csv_string + ","

    csv_string = csv_string + "\n"
    return csv_string


def get_time_from(line:str) -> datetime:
    """
    Given the line, get the timestamp
    :param line:
    :return: time stamp
    """

    timestamp_str = " ".join(line.split()[:-2])
    date_format = "%a %b %d %H:%M:%S UTC %Y"
    timestamp = datetime.datetime.strptime(timestamp_str, date_format)

    return timestamp


def main():
    # Make sure the locale is set to C.
    locale.setlocale(locale.LC_ALL, '')

    # Input file
    infile = "~/Documents/CU_Log/3-35759924091/dropbox/guds/3-35759924091/guds.004057fb.sn01AK00933775-2024.03.04.23.28.30/iostat.out"
    # infile = "~/Documents/CU_Log/3-35759924091/dropbox/guds/3-35759924091/guds.004057fb.sn01AK00933775-2024.03.04.23.28.30/iostat-test.out"
    input_file_path = path.abspath(path.expanduser(infile))

    # Output file
    outfile = "~/Downloads/iostat.csv"
    output_file_path = path.abspath(path.expanduser(outfile))

    # Prepare a Pandas Dataframe to store final data.
    column_names = ["time", "device", "r_s", "w_s", "kr_s", "kw_s",
                    "wait", "actv", "wsvc_t", "asvc_t", "pct_w", "pct_b"]
    column_names = ["time", "device", "r/s", "w/s", "kr/s", "kw/s",
                    "wait", "actv", "wsvc/t", "asvc/t", "pct/w", "pct/b"]

    str_to_write = csv_write(column_names)
    fdw = open(output_file_path, "w")
    fdw.write(str_to_write)

    # counters
    datapoints_processed = 0

    # Read the file and covert the data.
    with open(input_file_path, mode="r", encoding="utf-8") as fd:
        lines = fd.readlines()
        in_data = False
        for line in lines:
            line = line.rstrip("\n")

            # If null line, skip this line
            if len(line) == 0:
                continue

            # Data is between "started" and "ended" message.
            last_word = line.split()[-1]
            if last_word == "started":
                # get initial time stamp for the current set of data.
                datapoint_time = get_time_from(line)
                in_data = True
                continue

            if last_word == "ended":
                in_data = False
                continue

            # If we are not in dataset, skip lines
            if in_data is False:
                continue

            # if we get "extended device statistics" subtitle, skip the line.
            if "extended device statistics" in line:
                continue

            # Progress the datapoint clock by 1 second when we find
            # "device" at the beginning.  This is a header to indicate
            # the next 1 second of data.

            if line.split()[0] == "device":
                datapoint_time = datapoint_time + datetime.timedelta(seconds=1)
                continue

            # Process actual data and write
            datapoints = line.split()
            datapoint_time_str = datapoint_time.isoformat()
            datapoints.insert(0, datapoint_time_str)
            str_to_write = csv_write(datapoints)
            fdw.write(str_to_write)

            # Progress bar(?)
            if datapoints_processed % 100 == 0:
                print(f"Processed {datapoints_processed} records")

            datapoints_processed = datapoints_processed + 1

        # Clean up
        fdw.close()
        fd.close()
        print(f"Processed {datapoints_processed} records.")

if __name__ == '__main__':
    main()
