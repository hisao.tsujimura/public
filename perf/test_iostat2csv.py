# =========================================================================
#
#   Analysis Script
#
#   Script name: test_iostat2csv.py
#
#   What it does:
#       This is a test code for iostat2csv.py without using Pandas
#
#   Copyright 2024, by Hisao Tsujimura
#
# =========================================================================

import unittest
import iostat2csv as ic

class TestIostat2csv(unittest.TestCase):
    def test_csv_write_str1(self):
        column_names = ["time", "device", "r/s", "w/s", "kr/s", "kw/s",
                        "wait", "actv", "wsvc_t", "asvc_t", "%w", "%b"]
        expected = '"time", "device", "r/s", "w/s", "kr/s", "kw/s", "wait", "actv", "wsvc_t", "asvc_t", "%w", "%b"\n'

        result = ic.csv_write(column_names)
        assert result == expected

    def test_csv_write_mix1(self):
        data = "blkdev0   1.4   46.6   99.5 1061.2  0.0  0.0    0.0    0.1   0   0"
        expected = '"blkdev0", 1.4, 46.6, 99.5, 1061.2, 0.0, 0.0, 0.0, 0.1, 0, 0\n'
        result = ic.csv_write(data.split())
        assert result == expected

