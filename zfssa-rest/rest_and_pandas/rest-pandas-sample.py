# =======================================================================
#
# Sample Code for ZFSSA filesystems REST API call
#
# The script is a sample purposes only, and provided "as-is."
#
# Copyright 2021, by Oracle Corporation and its affiliates
#
# =======================================================================

#
# This script refers to the following product document.
# However, since it is based on Python2 and this code assumes Python3,
# I am using request package instead of urllib.  urllib3 is to suppress
# warning message.
#
# Reference: Oracle ZFS Storage Appliance RESTful API Guide, Release OS 8.8.x.
# "Python Restful Client"
# https://docs.oracle.com/cd/F13758_01/html/F13772/goqxa.html
#

import requests
import urllib3

# In this sample, we use Pandas to extract JSON into table, and then convert it
# to CSV format.

import pandas as pd

# other libraries we need
import sys
import pprint   # for debugging

# ---------------------------------------------------------------------------
# Main
# ---------------------------------------------------------------------------

# Before doing anything, make sure we don't use proxy.  We are in lab.
# By default, it reads what you have in the shell environment.
proxies = {
    "http": None,
    "https": None
}

# It's not a good practice to ignore certificate verification, but we are in lab.
ssl_verify = False
urllib3.disable_warnings()

print("*** Sample Code to Get file system information from ZFSSA ***")
user=input("Please input user name:")
password=input("Please input password:")
zfssa_node="vcap-sca-00002.us.oracle.com"
port=":215"

# While you can use REST API v2, for the sake of simplicity, it's v1.
print("Trying to connect REST v1 interface...")
uri = "https://" + zfssa_node + port + "/api/access/v1"
print(uri)

# It's not a good practice to ignore certificate verification, but we are in lab.
r = requests.get(uri, auth=(user, password), proxies=proxies, verify=ssl_verify)

if r.status_code > 400:
    print("HTTP Error status = " + str(r.status_code))
    sys.exit(1)
else:
    print("Success!")

#
# Get filesystem information from REST API, and covert it into CSV.
# We can do filtering by specifying slices in pandas.
#

print("Obtaining file system list...")
uri = "https://" + zfssa_node + port + "/api/storage/v1/filesystems"
print(uri)

response = requests.get(uri, auth=(user, password),
                 proxies=proxies, verify=ssl_verify) # Basic Auth for lab use
if response.status_code > 400:
    print("HTTP Error status = " + str(r.status_code))
    sys.exit(1)


print("* Converting response to Panda's dataframes")
outfile = "./rest_test.csv"
fs_outfile = "./freespace.csv"
print("  output file is at " + outfile)
# Get the response and convert into JSON.
# Since raw JSON is nested, remove nesting.
# You can change the output format for CSV with the arguments you pass
# to to_csv() method.  You need to make sure you add nas_rep to indicate
# how to handle NaN or missing values.

raw_json = response.json()
df = pd.json_normalize(raw_json, record_path=['filesystems'])
df.to_csv(outfile, na_rep="off")

# If you want to chose columns to write to CSV, you can specify as an
# option in to_csv() method.
# # df.to_csv(fs_outfile, columns=[""])
print("  free space file is at " + fs_outfile)
df.to_csv(fs_outfile, na_rep="off", columns=["canonical_name", "space_available"])

print("*** Complete")
