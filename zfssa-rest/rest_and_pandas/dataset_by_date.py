# ===========================================================================
#
#  Sample Program
#
#  This sample program is for demonstration purposes only.
#  It is provided "as-is."
#
#  What It Does:
#   This is a summation program of analytics dataset.
#
#   (1) It retrieves the analytics dataset from ZFS Storage
#   (2) Parse the dataset into Pandas format
#
#  Requirements:
#   It is testd with Python 3.11 and required libraries below.
#
#  Copyright 2024, by Hisao Tsujimura
#
# ===========================================================================

# Required libraries
import requests
import pandas as pd
import urllib3

# System libraries
import sys
import pprint

proxies = {
    "http": None,
    "https": None
}


# --------------------------------------------------------------------------
#  Helper Functions
# --------------------------------------------------------------------------
def disable_ssl_warnings():
    print("* Note: Disabling all SSL warnings for demo purpose.")
    urllib3.disable_warnings()


# --------------------------------------------------------------------------
#  Main
# --------------------------------------------------------------------------
def main():
    # Customize this to your ZFSSA name to test in your lab.
    # I am using replication dataset in this case.
    zfssa_hostname = "vcap-sca-00002.us.oracle.com"
    analytics_href = "/api/analytics/v2/datasets/repl.bytes[peer]"
    uri = "https://" + zfssa_hostname + ":215" + analytics_href

    print("** Download analytics dataset **")
    username = input('Please enter your user name:')
    password = input('Please enter the password:')

    # Refer to "Get Dataset" section of
    # the "Oracle ZFS Storage Appliance RESTful API Guide"
    print(f" - Retrieving meta data from {zfssa_hostname}")
    r = requests.get(uri, auth=(username, password), proxies=proxies, verify=False)

    if r.status_code > 200:
        print(f" ! ERROR HTTP status {r.status}")
        print(f" ! The requested URI was {uri}")
        sys.exit(3)

    metadata = r.json()
    pprint.pprint(metadata)

    # Refer to "Get Dataset Data" section of
    # the "Oracle ZFS Storage Appliance RESTful API Guide."
    start_time = metadata["dataset"]["since"]
    span = "day"
    granularity = "hour"

    data_specifier = f"/data?start={start_time}" + f"&span={span}" + f"&granularity={granularity}"
    uri = "https://" + zfssa_hostname + ":215" + analytics_href + data_specifier

    r = requests.get(uri, auth=(username, password), proxies=proxies, verify=False)

    if r.status_code > 200:
        print(f" ! ERROR HTTP status {r.status}")
        print(f" ! The requested URI was {uri}")
        sys.exit(3)

    # This is the base raw data for manipulation.
    # Since stat will come with the datapoints in the form of
    # 'data': {'max': 230, 'min': 0, 'value': 0}, we need to identify our
    # interest.

    raw_data = r.json()["data"]

    # Convert to Pandas dataframe, drop columns we don't need, and
    # expand the data in shape of dictionary to columns, and convert
    # time stamp into datetime type.
    # NOTE:  This part is "data dependent."  Therefore, you may have to modify
    #        the below parser to something else if you want to deal with a
    #        different dataset.

    print(" - Processing received data...")
    df = pd.DataFrame(raw_data)

    # The column "data" comes in {"min": xxx, "max": yyy, "value": vvv} format.
    # I need to expand this into the columns for a "flat" to create a dataframe.
    # So,
    # prepare another data frame to separate dataset, convert a dict into
    # columns, merge them with df, and drop unnecessary columns.
    
    print("   o Separating data into different columns...")
    df_data = df[["data", "sample"]]
    df_to_merge = pd.DataFrame(df_data,
                               columns=["data_min", "data_max", "data_value", "sample", "data"])

    for i in df_to_merge.index:
        d = df_to_merge.loc[i]["data"]
        df_to_merge.at[i, "data_min"] = d["min"]
        df_to_merge.at[i, "data_max"] = d["max"]
        df_to_merge.at[i, "data_value"] = d["value"]

    df = df.merge(df_to_merge, how="left", on="sample")
    pd.to_datetime(df["startTime"])

    # Now remove unnecessary columns
    df.drop(columns=["data_x", "data_y", "sample", "samples"], axis=1, inplace=True)
    print(df.columns)
    print("   o Complete.")

    print("Beginning of the data")
    print(df.head(5))


if __name__ == "__main__":
    main()
