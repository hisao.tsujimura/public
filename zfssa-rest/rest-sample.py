#===================================================================
#
#  REST API Sample
#
# What This Does:
#   This sample sends the following REST requests and receives
#   the answers from the server.
#
# Prerequisites:
#   - Python 3.6
#   - ZFS Storage Software 2013.06.05.7.17
#    (The above are tested versions.  The lower versions should work
#   as long as Python packages used here are available and ZFSSA
#   serves the same RESTful API.)
#
# Python Libraries:
#   If not installed in your environment, the following Python libraries
#   needs to be installed.
#       - urllib, pprint, json, urllib3
#
# Caveat
#   For the sake of showing example, this script:
#       - intentionally ignores certificates.
#
# This code is provided "as-is" and meant to provide a sample use case.
#
# Copyright (c) 2018 - 2019, Hisao Tsujimura
#
#========================================================================

# date/time support
from datetime import datetime

# REST Support
import pprint
import requests
import urllib3

#-------------------------------------------------------------------------
# Main
#-------------------------------------------------------------------------

def main():
    print("## Sample REST API by Python")

    print("Please enter user ID:")
    #user_name = input()
    print("Please enter password:")
    #password = input()

    user_name = "root"
    password = "handl3bar"

    # Generate the full URL for API from the host name.
    zfssa_hostname = "vcap-sca-00004.us.oracle.com"
    target_url = "https://" + zfssa_hostname + ":215/api/"

    # Some mess clean up such as warning and ignoring proxies
    print("* NoteDisabling SSL warnings for demo purposes.")
    urllib3.disable_warnings()
    print("* Disabling proxy setting for now.")
    proxies = {
        "http": None,
        "https": None,
    }

    #
    # Here are actual calls to ZFS Storage via REST API using "requests" package.
    # As you can see, I should probably put the message and requests.get into a
    # function, but for the ease of reading the code, I left as is.
    #
    # "response" comes back as a JSON object.  So you can use JSON package to
    # extract necessary items.
    #

    print(" - Login")
    # Get first access
    action = "access/v1"
    uri = target_url + action
    print("* Sending requests to " + uri)
    r = requests.get(uri, auth=(user_name, password), proxies = proxies, verify=False)
    print(r.status_code)

    # Browse Network Items
    print(" - Network")
    action = "network/v1"
    uri = target_url + action
    print("* Sending requests to " + uri)
    r = requests.get(uri, auth=(user_name, password), proxies = proxies, verify=False)
    pprint.pprint(r.json())


    # Browse all datalinks
    print(" - List Up All Datalinks")
    action = "network/v1/datalinks"
    uri = target_url + action
    print("* Sending requests to " + uri)
    r = requests.get(uri, auth=(user_name, password), proxies = proxies, verify=False)

    pprint.pprint(r.json())

    #
    # Here is a sample to get an item from a JSON object.
    # If the previous requests.get is successful, converted JSON object should have
    # dictionary should have an dictionary that starts like below.
    #   {'datalinks': [{'class': 'device',...
    # The object of the dictionary can be accessed as x["index"].
    # Since the object is a list, we can use a for loop to show them.
    #
    # A little more precisely, the object is a dictionary that has only 1 item
    # with an index "datalinks", whose value is a list.
    # The list items are also dictionary.  This one has multiple indices.
    #

    # Sample to get an element from a JSON object
    print("* Since items we want is a list in a dictionary with an index,")
    print("* I take out a lint and then display them using a for loop.")
    print("* {'datalinks': [{'class':  ....]}")

    r_json = r.json()
    network_items = r_json["datalinks"]

    for item in network_items:
        #print(item)
        print(item["label"])
    print("### Complete.")


if __name__ == "__main__":
    main()
