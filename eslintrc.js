module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    // "extends": "eslint:recommended",
    "extends": ["eslint"],
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        // always indent with 4 spaces
        "indent": [ "error", 4 ],
        // end of line is LF
        "linebreak-style": [
            "warn",
            "unix"
        ],
        // single quotes only
        "quotes": [
            "error",
            "single"
        ],
        // semi-colon at the end of the line
        "semi": [
            "error",
            "always"
        ],
        // brace style
        "brace-style": [
            "error",
            "stroustrup"
        ],

        // variable camelcase
        "camelcase": "warn", 

        // Use == for comparision, not ===
        "eqeqeq": "off",

        // allow to write "var xx = function"
        "func-style" : [
            "error",
            "expression"
        ],

        // Enforce line length and tab stop of 4 characters
        "max-len": [ "warn", { "code": 80, "tabWidth" :4}],
        // Do not care about the sequence of the functions and class in the code
        "no-use-before-define": ["error", { "functions": false, "classes": false }],

        // allow non-used variables, set it to warning, not error.
        "no-unused-vars": ["warn", { "vars": "all", "args": "after-used" }]
    }
};
