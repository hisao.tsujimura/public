#!/bin/ksh 
# 
# Network Topology Mapper
# 
# This script uses tshark to extract the following information
# from the packet capture.  Then, it converts the data to DOT
# format.
#
# You need to open this with DOT compatible software such 
# as Graphviz.
#
#* Version 1.0 / Hisao Tsujimura
#
#* Known Bugs
#  IPoIB packets are not interpreted correctly. (Future Enhancement)
#--------------------------------------------------------------
# variables
#--------------------------------------------------------------
## DEBUG='true'	# debug flag
INFILE=~/Dropbox/*.out 
LOGFILE='./ntm.log'	# logfile
TEMPDIR=$HOME'/temp2'
TEMPFILE=$TEMPDIR/tempfile.tmp
TEMPFILE1=$TEMPDIR/tempfile1.tmp
TEMPFILE2=$TEMPDIR/tempfile2.tmp
TEMPFILE3=$TEMPDIR/tempfile3.tmp
TEMPFILE4=$TEMPDIR/tempfile4.tmp
TSHARK_OUT=$TEMPDIR'/tshark-e.out'
MAC_LIST=$TEMPDIR'/macaddress.out'
IP_LIST=$TEMPDIR'/ipaddress.out'
CONV_LIST=$TEMPDIR'/conv.out'
OUTFILE='./map.dot'
#--------------------------------------------------------------
# functions
#--------------------------------------------------------------
# function : debug()
# arguments: string: string to keep record
# output   : string preced by ### DEBUG -  
debug()
{
	case $DEBUG in
	'true')
		echo '### DEBUG - '$*
		;;
	esac
}

# function : log()
# arguments: string: a string with any number of fields
# output   : time stamped string to the file specified by LOGFILE

log()
{
	OUTSTRING=`date +%y%m%d-%T`
	echo $OUTSTRING' '$* >> $LOGFILE
	debug $OUTSTRING' '$*
	echo  $OUTSTRING' '$*
}

# function : prep
# arguments: none
# output   : none 
prep()
{
	log 'creating work directory.'
	mkdir -p $TEMPDIR
	rm $OUTFILE
}

# function : parse_packets 
# arguments: $1 - path to packet file
# output   : parsed packets with -V option
parse_packets()
{
	INFILE=$1
	log 'parsing packet...'
	log '  infile='$INFILE
	##tshark -tad -V -r $INFILE > $TSHARK_OUT
	tshark -tad -r $INFILE -Tfields -e eth.src -e ip.src -e eth.dst -e ip.dst | sort -u > $TSHARK_OUT
}

# function : make_mac_list 
# arguments: none 
# output   : $MAC_LIST
make_mac_list()
{
	cat $TSHARK_OUT | awk '{ if (NF == 4) printf("%s\n%s\n",$1,$3); if (NF == 2) printf("%s\n%s\n",$1,$2);}' |
		sort -u > $MAC_LIST
}


# function : make_ip_list 
# arguments: none 
# output   : $IP_LIST
make_ip_list()
{
	log 'making IP address list...'
	cat $TSHARK_OUT | awk '{ if (NF == 4) printf("%s\n%s\n",$2,$4);}' |
		sort -u > $IP_LIST
}

#--------------------------------------------------------------
# DOT
#--------------------------------------------------------------
# function : dot_write()
# arguments: string to write to DOT file
# output   : string to the $OUTPUT file
dot_write()
{
	echo $* >> $OUTFILE
}

# function : dot_graph_begin()
# arguments: none 
# output   : start of graph to $OUTPUT file
dot_graph_begin()
{
log 'dot: new graph' 
dot_write 'digraph {'
}

# function : dot_graph_end()
# arguments: none 
# output   : start of graph to $OUTPUT file
dot_graph_end()
{
log 'dot: end graph' 
dot_write '} // end of graph'
}

# function : dot_create_entry()
# arguments: none 
# output   : make an entry in dot 
dot_create_entry()
{
log ' - dot entry :'$1 
##dot_write '"'$1'" [shape=box, fillcolor=blue, xlabel="'$1'"]'
dot_write '"'$1'" [shape=box, fillcolor=blue]'
}

#--------------------------------------------------------------
# main
#--------------------------------------------------------------
clear
case $1 in 
'')
	continue;;
*)
	INFILE=$1
	;;
esac

prep
parse_packets $INFILE
make_mac_list 
make_ip_list 
#make_conversation_list

dot_graph_begin

log 'writing MAC address entries...'
for i in `cat $MAC_LIST | sort | uniq` 
do
	dot_create_entry $i
done

log 'writing IP address entries...'
for i in `cat $IP_LIST | sort | uniq`
do
	dot_create_entry $i
done

rm $TEMPFILE1 2> /dev/null
rm $TEMPFILE2 2> /dev/null
rm $TEMPFILE3 2> /dev/null
rm $TEMPFILE4 2> /dev/null
log 'writing conversation'
while read LINE
do
	NF=`echo $LINE | awk '{printf("%s",NF);}'`

	case $NF in
	2)
		##OUTSTR=`echo $LINE | awk '{printf("\"%s\" ->\ "%s\" [color=blue,style=solid,penwidth=1];",$1,$2);}'`
		OUTSTR=`echo $LINE | awk '{printf("\"%s\" -> \"%s\" [color=blue,style=solid,penwidth=1];",$1,$2);}'`
		echo $OUTSTR >> $OUTFILE
		;;
	4)
		# conversation by mac address 
		OUTSTR1=`echo $LINE | awk '{printf("\"%s\" -> \"%s\" [color=blue,style=solid,penwidth=1];",$1,$3);}'`
		# conversation by IP addresss
		OUTSTR2=`echo $LINE | awk '{printf("\"%s\" -> \"%s\" [color=red,style=solid,penwidth=1];",$2,$4);}'`
		# MAC - IP map (source) 
		OUTSTR3=`echo $LINE | awk '{printf("\"%s\" -> \"%s\" [color=black,style=solid,penwidth=1];",$1,$2);}'`
		# MAC - IP map (source) 
		OUTSTR4=`echo $LINE | awk '{printf("\"%s\" -> \"%s\" [color=black,style=solid,penwidth=1];",$3,$4);}'`
		echo $OUTSTR1 >> $TEMPFILE1
		echo $OUTSTR2 >> $TEMPFILE2
		echo $OUTSTR3 >> $TEMPFILE3
		echo $OUTSTR4 >> $TEMPFILE4
		;;
	esac

done < $TSHARK_OUT

log 'writing conversation to the file ('$OUTFILE')...'
dot_write '// conversation'
log ' - mac converstion'
dot_write '// mac conversation'
cat $TEMPFILE1 | sort -u >> $OUTFILE
log ' - ip  converstion'
dot_write '// ip  conversation'
cat $TEMPFILE2 | sort -u >> $OUTFILE
log ' - ip - mac map'
dot_write '// ip  - mac mapping'
cat $TEMPFILE3 $TEMPFILE4 | sort -u >> $OUTFILE
dot_write '// end of conversation'
dot_graph_end

##log 'cleaning up work directory.'
##rm -rf $TEMPDIR

## check if dot is available
STAT=`which dot`
case $STAT in 
'')
	# dot is not here, do nothing
	continue
	;;
*)
	log 'converting '$OUTFILE' to PDF.'
	dot -Tpdf $OUTFILE > $OUTFILE.pdf
	;;
esac
log 'complete.'
