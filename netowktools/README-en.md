# README.md (EN)

### topology_mapper.sh

This script runs Wirehark (tshark) against a packet capture file that tshark can read and takes its result and covert it into the DOT file.
By running this script, you can visualize the relationship between MAC addresses and IP addresses as well as their conversation.
When dot is installed, this script converts the generated file into a PDF file.  The resolusion of PDF file depends on the visualization libraries in your environment.

#### Prerequisits

1) tshark works correctly.
2) We have AT&T GraphViz DOT installed such as in /bin/dot 

#### Known Prolbem 

1) It is not tested under IPv6 environment
2) It is not tested under Infiband environment. 

#### How This Works

The script will call tshark to generate each list.
Currently, tshark is called serially, therefore, it takes 3 times the time than calling tshark once.

a) Generate MAC address list
b) IP address list (To make a conversation list)
c) IP/MAC conversation list

d) Generate a DOT file based on the output from a) - c)
e) If we have /bin/dot or equivalent, we convert the file into a PDF.

__end of README-en.md__
