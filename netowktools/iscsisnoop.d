#!/usr/sbin/dtrace -Cs
/*
 * iscsisnoop.d - Snoop iSCSI events. Solaris Nevada, DTrace.
 *
 * This snoops iSCSI events when run on an iSCSI server.
 *
 * USAGE: iscsisnoop.d          # Hit Ctrl-C to end
 *
 * FIELDS:
 *              CPU             CPU event occured on
 *              REMOTE IP       IP address of the client
 *              EVENT           Data I/O event (data-send/data-receive)
 *              BYTES           Data bytes
 *              ITT             Initiator task tag
 *              SCSIOP          SCSI opcode as a description, as hex, or '-'
 *
 * NOTE: On multi-CPU servers output may not be in correct time order
 * (shuffled). A change in the CPU column is a hint that this happened.
 * If this is a problem, print an extra timestamp field and post sort.
 */

#pragma ident   "@(#)iscsisnoop.d       1.2     07/03/27 SMI"

#pragma D option quiet
#pragma D option switchrate=10

/* 
 * If you want NOP to be displayed in the output, please set SHOW_NOP to B_TRUE.
 * The default is B_FALSE.
 */

#define B_TRUE  1
#define B_FALSE 0

#define SHOW_NOP    B_FALSE

dtrace:::BEGIN
{
        printf("%-20s %-14s %3s  %-15s %-15s %6s %10s  %6s\n", 
                "WALLTIME",
                "TIME(ns)",
                "CPU", "REMOTE IP",
            "EVENT", "BYTES", "ITT", "SCSIOP");

        /*
         * SCSI opcode to string translation hash. This is from
         * /usr/include/sys/scsi/generic/commands.h. If you would
         * rather all hex, comment this out.
         */
        /* modified by Hisao Tsujimura
         * Added some commands that can conform large data-in,
         * as well as some historic commands.  If you have no
         * idea of what "write filemarks" is, you must be a disk 
         * guy. :-) 
         * The below commands are based on SPC-5 working draft revision 13, 
         * dated November 19, 2016 by INCITS. 
         */
        scsiop[0x00] = "test unit-ready";
        scsiop[0x01] = "rewind";
        scsiop[0x08] = "read(6)";
        scsiop[0x0a] = "write(6)";
        scsiop[0x0b] = "seek";
        scsiop[0x0f] = "read reserve";
        scsiop[0x10] = "write filemarks(6)";
        scsiop[0x12] = "inqury";
        scsiop[0x17] = "release";
        scsiop[0x1a] = "mode sense(6)";
        scsiop[0x1b] = "start/stop unit";
        scsiop[0x28] = "read capacity";
        scsiop[0x28] = "read(10)";
        scsiop[0x2a] = "write(10)";
        scsiop[0x2b] = "seek(10)";
        scsiop[0x2e] = "write and verify (10)";
        scsiop[0x3f] = "write long(10)";
        scsiop[0x40] = "write same(10)";
        scsiop[0x4d] = "log sense";
        scsiop[0x5e] = "persistent reserve in";
        scsiop[0x5f] = "persistent reserve out";
        scsiop[0x81] = "read reserve(16)";
        scsiop[0x88] = "read(16)";
        scsiop[0x8a] = "write(16)";
        scsiop[0x8e] = "write and verify(16)";
        scsiop[0x9e] = "report bind group (0x9e)";
        scsiop[0x9f] = "bind (0x9f)";
        scsiop[0xa0] = "report lun";
        scsiop[0xa3] = "management protcol in  (0xa3)";
        scsiop[0xa4] = "management protcol out (0xa4)";
        scsiop[0xa5] = "move medium";
        scsiop[0xa8] = "read(12)";
        scsiop[0xb8] = "read element status";
}

iscsi*:::data-*,
iscsi*:::login-*,
iscsi*:::logout-*,
iscsi*:::task-*,
iscsi*:::async-*,
iscsi*:::scsi-response
{
        printf("%Y %14d %3d  %-15s %-15s %6d %10d -\n", 
                walltimestamp, timestamp,
                cpu, args[0]->ci_remote,
            probename, args[1]->ii_datalen, args[1]->ii_itt);
}

iscsi*:::scsi-command
/scsiop[args[2]->ic_cdb[0]] != NULL/
{
        printf("%Y %14d %3d  %-15s %-15s %6d %10d  %s\n", 
                walltimestamp, timestamp,
                cpu, args[0]->ci_remote,
            probename, args[1]->ii_datalen, args[1]->ii_itt,
            scsiop[args[2]->ic_cdb[0]]);
}

iscsi*:::scsi-command
/scsiop[args[2]->ic_cdb[0]] == NULL/
{
        printf("%Y %14d %3d  %-15s %-15s %6d %10d  0x%x\n", 
                walltimestamp,timestamp,
                cpu, args[0]->ci_remote,
            probename, args[1]->ii_datalen, args[1]->ii_itt,
            args[2]->ic_cdb[0]);
}

/*
 * For NOP
 */
iscsi*:::nop-* 
/SHOW_NOP == B_TRUE /
{
        printf("%Y %14d %3d  %-15s %-15s %6d %10d -\n", 
                walltimestamp, timestamp,
                cpu, args[0]->ci_remote,
            probename, args[1]->ii_datalen, args[1]->ii_itt);
}
