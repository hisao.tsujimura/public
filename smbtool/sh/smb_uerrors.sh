#!/bin/ksh
#=============================================================================
# 
# SMB Log Analysis
# 
# script name: smb_uerrors.sh
#
# Usage:
#   smb_uerrors.sh <single file name>
#
# 2017-02-02 (c) Hisao Tsujimura
#
#=============================================================================
#
# Default Values
#

INFILE='./debug.sys'    # default file name to work on
AWK=`which awk`         # path to AWK

#-------------------------------------- 
# main 
#-------------------------------------- 
# Things we want to keep record of
# 
# NO_SUCH_USER
#   1) User name
#   2) Error Count
#   3) First event date/time
#
# We make ns_array indexed by user name
#   ns_array[n,0] -> user name
#   ns_array[n,1] -> error count
#   ns_array[n,2] -> first event date/time

$AWK 'BEGIN{
        ns_user_count=0;    # number of unique users
        ns_lines=0;         # lines processed for NO_SUCH_USERS
        B_FALSE=0;  B_TRUE=1;    #constant for flags
        F_DUMP=B_FALSE;          #dump content of debug array 
        F_DUMP2=B_FALSE;         #dump content of debug array 
        F_DEBUG=B_FALSE1;          #debug flag

        LINE_INDICATOR=10000;   # number of lines when it says something
    }

/NO_SUCH_USER/{
        c_user_name=substr($9,7,length($9)-9);
        ns_lines=ns_lines+1;

        if ( ns_lines % LINE_INDICATOR == 0 )
            printf("%d lines processed...\n",ns_lines);

        # search if the user was already found
        # linear search O(N)

        F_FOUND=B_FALSE;
        ns_pos=0;   # search from index 0

        while ( ns_pos <= ns_user_count )
        {
            # if found, set a flag, and remeber the index. 
            if ( ns_array[ns_pos,0] == c_user_name )
            {
                F_FOUND=B_TRUE;
                found_pos=ns_pos;
            }

            ns_pos=ns_pos+1;
        }

        # big debug code

        if ( F_DEBUG == B_TRUE )
        {
            printf("## DEBUG %s\n",$0);
            printf("## DEBUG $9=%s, c_user_name=%s\n",$9,c_user_name);
            printf("## DEBUG found=%d, found_pos=%d\n",F_FOUND,found_pos);
            printf("## DEBUG ns_user_count = %d\n",ns_user_count);
        }

        #  dump content of array
                    
        if ( F_DUMP == B_TRUE )
        {
            printf("## ---- Array ----\n");
            i=0;
            while ( i <= ns_user_count)
            {
                printf("## DEBUG index, name, error_count, start_date\n"); 
                printf("## DEBUG %d\n%s\n%s\n%s\n",
                    i,
                    ns_array[i,0],
                    ns_array[i,1],
                    ns_array[i,2]);
                i=i+1;
            }
        }

        # if not found, load it up.
        if ( F_FOUND == B_FALSE )
            {
                ns_array[ns_pos,0]=c_user_name;
                ns_array[ns_pos,1]=1;
                ns_array[ns_pos,2]=sprintf("%s %s %s",$1,$2,$3);
                ns_user_count = ns_user_count + 1;       
            }

        # if found, count it up.
        if ( F_FOUND == B_TRUE )
            {
                ns_array[found_pos,1]=ns_array[found_pos,1] + 1;
            }


        #  dump content of array
                    
        if ( F_DUMP2 == B_TRUE )
        {
            printf("## ---- Array 2 ----\n");
            i=0;
            while ( i <= ns_user_count)
            {
                printf("## DEBUG index, name, error_count, start_date\n"); 
                printf("## DEBUG %d\n%s\n%s\n%s\n",
                    i,
                    ns_array[i,0],
                    ns_array[i,1],
                    ns_array[i,2]);
                i=i+1;
            }
        }
    }

    END{
        # print out everything

        printf("========== Summary of SMB Error ==========\n");
        printf("NO_SUCH_USERS events = %d\n",ns_lines);

        i=1;
        while ( i <= ns_user_count )
        {
            printf("NO_SUCH_USER: name = %-32s count=%d first event =%s\n",
            ns_array[i,0],
            ns_array[i,1],
            ns_array[i,2]);

            i=i+1;
        }
    } 
' $1

