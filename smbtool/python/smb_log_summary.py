#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
# --------------------------------------------------------------
#
# SMB Tools
#
# Script name: smb_log_summary.py
#
# How To Run:
#   smb_log_summary.py [-i|--i <filename for analysis>]
#
# What This Does:
#
#   It looks for certain strings to in the message file
#   and obtain the followings.
#     (1) The first event date/time and its message
#     (2) The last event date/time and its message
#     (3) The event type and its count
#     (4) The raw event log for the first event
#     (5) Count of events by users for the following events
#         WRONG_PASSWORD
#         NO_SUCH_USER
#         The current version prints only the top 5 since
#         in some environment, the number of users can be large.
#
# Keywords to Find In Log:
#
#   The following keywords are also looked at:
#
# keywords = ['NO_SUCH_USER', 'WRONG_PASSWORD', 'ACCOUNT_EXPIRED',
#            'smb_kdoor_chkhdr', 'smb_kdoor_upcall',
#            'ndr_rpc_bind', 'smbd_opipe_exec', 'Resource temporarily unavailable',
#            'domain service not responding', 'SID-to-name', 'SMBIOC_T2RQ failed',
#            'smb_smb_treeconnect', 'Connection to Active Directory server failed']
#
#   Note: You can specify the keywords in form of a "list."
#
#  This program does not filter message by time stamp yet.
#
# * To Do:
#   Add filter-by-time feature
#
# Copyright 2017, 2018 Hisao Tsujimura
# * Revision
#   2018-02-22 1.3.1
#   2020-10-22 1.3.2 - add access deny summary
#
# --------------------------------------------------------------

import argparse
# date/time support
from datetime import datetime

#----------------------------------------------------------------------------
# Exception Handling
#----------------------------------------------------------------------------

# List of Errors To Handle

E_NULLARG = "Empty parameter passed"
E_NOT_STRING = "The parameter is not a string."
E_NOT_NUMBER = "The parameter is not a number."
E_DICT_INVALID="The dictionary name specified is invalid."
E_DICT_EMPTY="The specified dictionary is empty."
E_DICT_DUPKEY="A duplicate key specified when adding value to the dictionary."


def handle_exception(func_name, message):
    raise Exception(func_name + ': ' + message)

#----------------------------------------------------------------------------
#  Access Denied Log Statistics
#----------------------------------------------------------------------------

class acess_deny_stat(object):
    def __init__(self, name="<not set>"):
        self.name = name
        self.date_str = ""  # Date of Occurrence in string
        self.user = ""  # The user name in string
        self.ip = ""    # The IP address in string
        self.count = 0  # The count of events

    def __eq__(self, other):
        if type(self) != acess_deny_stat or type(other) != acess_deny_stat:
            return False

        return (
            self.name == other.name
            and
            self.date_str == other.date_str
            and
            self.ip == other.ip
        )

    def __lt__(self, other):    # for sorting
        # check user name and IP for comparison

        # if either one has user name None then, it's smaller.
        if self.user is None:
            return False
        elif other.user is None:
            return True

        if self.user < other.user:
            return False
        elif self.user > other.user:
            return True

        # Here, self and other have the same value in user.
        # Let's compare "IP" property

        if self.ip is None:
            return False
        elif other.ip is None:
            return True

        if self.ip < other.ip:
            return True
        else:
            return False

    def __hash__(self):
        hash(self.user and self.ip)

#----------------------------------------------------------------------------
# Dictionary Functions
#----------------------------------------------------------------------------

# functions for generating dictionary keys


def make_key_for_first_event(event_type):
    func_name="make_key_for_first_event()"
    key_for_first_event = None

    if (event_type == "Unknown") or (event_type == ""):
        handle_exception(func_name, E_NULLARG)
    if not event_type.isalpha():
        key_for_first_event = event_type + "_first"
    else:
        handle_exception(func_name,E_NOT_STRING)

    return(key_for_first_event)


def make_key_for_last_event(event_type):
    func_name="make_key_for_last_event()"
    key_for_last_event = None

    if ( event_type == "Unknown") or (event_type == ""):
        handle_exception(func_name, E_NULLARG)
    if not event_type.isalpha():
        key_for_last_event = event_type + "_last"
    else:
        handle_exception(func_name,E_NOT_STRING)

    return(key_for_last_event)


def make_key_for_event_count(event_type):
    func_name="make_key_for_event_count()"
    key_for_event_count = None

    if (event_type == "Unknown") or (event_type == ""):
        handle_exception(func_name, E_NULLARG)
    if not event_type.isalpha():
        key_for_event_count = event_type + "_count"
    else:
        handle_exception(func_name,E_NOT_STRING)

    return(key_for_event_count)


def make_key_for_event_label(event_type):
    func_name="make_key_for_event_label()"
    key_for_event_label = None

    if (event_type == "Unknown") or (event_type == ""):
        handle_exception(func_name, E_NULLARG)
    if not event_type.isalpha():
        key_for_event_label = event_type + "_label"
    else:
        handle_exception(func_name,E_NOT_STRING)

    return(key_for_event_label)


def make_key_for_raw_event(event_type):
    func_name="make_key_for_event_raw()"
    key_for_event_raw = None

    if (event_type == "Unknown") or (event_type == ""):
        handle_exception(func_name, E_NULLARG)
    if not event_type.isalpha():
        key_for_event_raw = event_type + "_raw"
    else:
        handle_exception(func_name,E_NOT_STRING)

    return(key_for_event_raw)

# Dictionary operations

#
# Get a value against a key
#


def get_value_for(d, key):

    func_name="get_value_for()"
    if (d == "" or d == "Unknown"):
        handle_exception(func_name, E_DICT_INVALID)

    if (len(d) == 0):
        return(None)

    if ( key in d):
        return(d[key])
    else:
        return(None)

#
# Add name, value pair into the specified dictionary
# It returns an updated dictionary
#


def add_key_value(d, key, value):

    func_name="add_key_value()"

    # If the dictionary size is zero (empty) add the key-value
    # without questioning.

    if len(d) == 0:
        d[key]=value
        return(d)

    # Check if the key already exist in the dictionary.
    # If there is, the caller needs to update instead of adding value,
    # so thrown an exception.

    if key in d:
        handle_exception(func_name, E_DICT_DUPKEY)

    # We add key-value when duplicate key is not found.
    d[key]=value

    return(d)

#
# Pretty Print Dictionary
#


def pretty_print(d):

    if len(d) == 0:
        print(E_DICT_EMPTY)

    for key in d.keys():
        print("Key: {0:<40}".format(key) + " Value: {0:>15}".format(str(d[key])))

#
# Count Up Event
#


def add_event(d, key):

    func_name = "add_event()"
    # if the event does not exist, set the value to one
    # if there is, update it.
    if not key in d:
        d[key] = 1
    else:
        last_count=get_value_for(d,key)

        if not str(last_count).isdigit():
            handle_exception(func_name, E_NOT_NUMBER)
            return
        else:
            new_count = last_count + 1
            d[key]= new_count

    return(d)

#----------------------------------------------------------------------------
# Processing A Line
#----------------------------------------------------------------------------
# get time stamp of current line
def get_timestamp(line):
    if len(line) != 0:
        return(line[0:15])
    else:
        return('MMM DD HH:MM:SS')


# get user name from the current line
def get_user_name(line):
    # in case of smbd message in Solaris, n = 8
    n=8

    tokens = line.split(' ')
    userinfo = str(tokens[n])

    posLeftBracket = userinfo.find('[')
    posRightBracket = userinfo.find(']')

    username = str(userinfo[posLeftBracket + 1:posRightBracket])

    return (username)


# Take the keyword and if the first event was never recorded,
# keep the time stamp and store it in the dictionary
def set_first_event(line, d, keyword):

    if keyword in line:
        key=make_key_for_first_event(keyword)

        if get_value_for(d, key) is None:
            timestamp = get_timestamp(line)
            d[key] = timestamp
            key_raw_event=make_key_for_raw_event(keyword)
            d[key_raw_event]=line

    return(d)


# Take the keyword and store the timestamp for te last event.
def set_last_event(line, d, keyword):

    if keyword in line:
        key=make_key_for_last_event(keyword)

        timestamp = get_timestamp(line)
        d[key]= timestamp

    return(d)


# Count the event up
def countup_event(line, d, keyword):

    if keyword in line:

        key=make_key_for_event_count(keyword)

        if key in d:
            d[key] = d[key] + 1
        else:
            d[key] = 1

    return(d)


def check_line_and_update(line, d, keyword):
    if len(line) == 0:
        return(d)

    set_first_event(line, d, keyword)
    set_last_event(line, d, keyword)
    countup_event(line, d, keyword)

    return(d)

# Count event by user, but only for two types of events


def update_user_event_count(line, d, keyword):

    # In order for the event count by user, we need to check
    # 1) If the keyword is the the supporetd one.
    # 2) If the line has the keyword

    if  not (keyword == "WRONG_PASSWORD" or keyword == "NO_SUCH_USER"):
        # print("## DEBUG: keyword is neither WRONG_PASSWORED or NO_SUCH_USER")
        return(d)

    if keyword not in line:
        # print("## DEBUG: line does not have " + keyword)
        return(d)

    # Now obtain the user name and count up.

    username=get_user_name(line)

    add_event(d,username)

    # done.  return an updated dictionary.
    return(d)

# Count up the event based on the current keyword.
# The post fix of the key will be _count


def update_smb_event_count(line, d, keyword):

    # Avoid space in list's indices
    key_for_counter=keyword.replace(" ", "_") + "_counter"

    # don't touch anything if the specified keyword is not found.
    if keyword not in line:
        return(d)

    add_event(d, key_for_counter)

    return(d)

#----------------------------------------------------------------------------
# Argument Functions
#----------------------------------------------------------------------------

def get_args():

    parser = argparse.ArgumentParser()

    parser.add_argument('-i', action="store", dest='infile',
                        help='path to the log file')
    parser.add_argument('--infile', action="store", dest='infile',
                            help='path to the log file')

    # time filter support
    parser.add_argument('-t', action="store", dest='start_time',
                        help='Specify the date/time format in ' +
                        'either YYYY-MM-DD or YYYY-MM-DD HH:mm:ss')
    parser.add_argument('--start-time', action="store", dest='start_time',
                        help='Specify the date/time format in ' +
                             'either YYYY-MM-DD or YYYY-MM-DD HH:mm:ss')
    parser.add_argument('-T', action="store", dest='end_time',
                        help='Specify the date/time format in ' +
                             'either YYYY-MM-DD or YYYY-MM-DD HH:mm:ss')
    parser.add_argument('--end-time', action="store", dest='end_time',
                        help='Specify the date/time format in ' +
                             'either YYYY-MM-DD or YYYY-MM-DD HH:mm:ss')

    results = parser.parse_args()

    return (results)

#-------------------------------------------------------------------------
# Making Reports
#-------------------------------------------------------------------------

# print top N items by count
def print_top_n(d,n):
    max_count = n
    current_count = 0

    for k, v in sorted(d.items(), key=lambda x:x[1], reverse=True):
        if current_count < max_count:
            print("Username = {0:30}".format(str(k)) +
                  "Count= {0:>6}".format(str(v)))
            current_count = current_count + 1

    return


# print events order by the keyword
def print_events_by_keyword(d,keywords):

    print("======= Event Counts by Type of Events =======")
    for k in keywords:

        print("Event type: {}".format(str(k)))
        key_first_event=make_key_for_first_event(k)

        # if the key for first event is not there,
        # we have no record of this event, so skip it by breaking the loop.
        if key_first_event not in d:
            print("  No events found for this event type.")
            print("")
            break

        key_first_event=make_key_for_first_event(k)
        print("  1st  Event at {}".format(d[key_first_event]))
        key_last_event=make_key_for_last_event(k)
        print("  Last Event at {}".format(d[key_last_event]))
        if k is not "NO_SUCH_USER" and k is not "WRONG_PASSWORD":
            key_counter = k + "_counter"
            print("  Event count = {}".format(d[key_counter]))
        key_raw_event = make_key_for_raw_event(k).rstrip("\n")
        print("  First event message at {}".format(d[key_raw_event]))

    return

#-------------------------------------------------------------------------
# Date Conversion Functions For Filter Support
#-------------------------------------------------------------------------
# We assume the date come in the below format in the log.
# Jan 11 19:27:45

# We don't have year included in the log, so we have to get the current
# year from the system.  I need to return this asa string as I need to
# use it to formulate a full date in other place of the program.

def get_current_year():
    now=datetime.now()

    return(str(now.year))


# Given the line, get time and return datetime variable
# However, because the input does not have year in the log file,
# we have to guess from the current time.
def get_event_time(line):

    log_timestamp = get_timestamp(line)
    log_timestamp_token=log_timestamp.split()

    this_year = get_current_year() #year
    log_month_name=log_timestamp_token[0] #month name
    log_day=log_timestamp_token[1]
    if len(log_day) == 1:
        log_day = "0" + log_day #if not zero filled day, fill.
    log_time=log_timestamp_token[2] #time  HH:MM:SS 24 hour format

    # get the string together and covert to datetime type.
    log_time_str= log_month_name + " " + log_day + " " + str(this_year) + " " + log_time
    log_time = datetime.strptime(log_time_str, "%b %d %Y %H:%M:%S")

    return(log_time)


# Given the format in either YYYY-MM-DD HH:MM:SS or YYYY/MM/DD HH:MM:SS,
# get the datetime type variable back.
def get_filter_time(date_time_string="1970/01/01 00:00:00"):

    # if the passed string contains a slash, it means the date is
    # slash delimited.  If not, it is a hyphen delimited.
    # Documentation does not specify if strptime method understands
    # the date format without 0 padded numbers, but we hope the best.

    if date_time_string == None:
        return("1970/01/01 00:00:00")

    if "/" in date_time_string:
        filter_time = datetime.strptime(date_time_string, "%Y/%m/%d %H:%M:%S")
    else:
        filter_time = datetime.strptime(date_time_string, "%Y-%m-%d %H:%M:%S")

    return(filter_time)


# Display filter conditions
def display_filter_criteria(start_time=None, end_time=None):
    if start_time is None or start_time == "1970/01/01 00:00:00":
        print(" - Start time = Not set.")
    else:
        print(" - Start time = {}".format(start_time))
    if end_time is None or end_time == "1970/01/01 00:00:00":
        print(" - End   time = Not set.")
    else:
        print(" - End   time = {}".format(end_time))



#-------------------------------------------------------------------------
# Main
#-------------------------------------------------------------------------


def main():

    # Initialize dictionaries per event types

    # We will store the first and last event and the event count in the same
    # dictionary.  For this purpose, we derive the key from the event names,
    # and prepare a dictionary.

    dict_smb_events=dict()

    # We will store event counts by user in the dictionary below.

    dict_wrong_password=dict()
    dict_no_such_users=dict()
    dict_locked_user=dict()

    # For counting access denied message
    list_access_denied = list()

    # Put event's key words in list such as NO_SUCH_USER or WRONG_PASSWORD

    keywords = [ 'NO_SUCH_USER', 'WRONG_PASSWORD', 'LOGON_FAILURE',
                 'E_SMBC',
                 'ACCOUNT_LOCKED_OUT',
                 'smb_kdoor_chkhdr', 'smb_kdoor_upcall',
                 'ndr_rpc_bind', 'smbd_opipe_exec', 'Resource temporarily unavailable',
                 'domain service not responding', 'SID-to-name', 'SMBIOC_T2RQ failed',
                 'smb_smb_treeconnect', 'Connection to Active Directory server failed',
                 'Device busy',
                 'ERROR\] cmd=SMB2',
                 'ticket time',
                 'guest disabled',
                 'access denied'
                ]

    # Get start time and end time.

    params = get_args()

    # Filter Support
    F_STRAT_FILTER = False
    F_END_FILTER = False

    start_time_str = params.start_time
    end_time_str = params.end_time

    if start_time_str == None:
        filter_start_time = get_filter_time(None)
    else:
        F_STRAT_FILTER = True
        filter_start_time = get_filter_time(start_time_str)

    if end_time_str == None:
        filter_end_time = get_filter_time(None)
    else:
        F_END_FILTER = True
        filter_end_time = get_filter_time(end_time_str)

    # /Filter Support

    # Title

    print("========== SMB Log Parser for debug.sys =========\n\n")
    display_filter_criteria(filter_start_time, filter_end_time)

    # get the file name to read
    filename = params.infile
    fd = open(filename)

    line_ctr = 0

    # read each line and count as necessary

    while True:
        line = fd.readline()

        if not line:
            break

        # Filter support
        # If the time stamp for the line is smaller than start time or
        # larger than end time, then, continue the loop.
        # (You cannot use break since we have two criteria.)
        # The below looks doing nothing, but actually skipping the loop if out of range.

        event_time = get_event_time(line)

        if F_STRAT_FILTER:
            if event_time < filter_start_time:
                continue

        if F_END_FILTER:
            if event_time > filter_end_time:
                break

        #
        # Access Denied Log
        # We want the summary of SMB "access denied" log every day.
        # We are interested in user name and the IP addresses.
        # We do not care the hours of the day.
        #

        # Sep 23 04:48:41 <server> smbsrv: [ID 709102 kern.notice] NOTICE: [<domain>\user]: (<ip>) <share> access denied: guest disabled

        if "access denied:" in line:
            fields = line.split()
            date_str = fields[0:2]
            user = fields[9]
            ip = fields[10]
            share = fields[11]

            new_event = acess_deny_stat()
            new_event.date_str = date_str
            new_event.user = user
            new_event.ip = ip
            new_event.share = share

            # Search if the event already exists.  If so, count it up.
            found = False
            for stat in list_access_denied:
                if stat == new_event:
                    stat.count = stat.count + 1
                    found = True

            if found is False:
                new_event.count = 1
                list_access_denied.append(new_event)

        # For debugging.  A large input file takes time to test.
        # max_line_count = 20000000
        # if line_ctr >= max_line_count:
        #    break

        for keyword in keywords:

            # The below function checks the event message, stores first and last event time stamp
            # as well as the raw message of the first event found.
            dict_smb_events=check_line_and_update(line, dict_smb_events,keyword)

            # The below are for counting up events
            if keyword == "WRONG_PASSWORD":
                dict_wrong_password=update_user_event_count(line, dict_wrong_password, keyword)
            elif keyword == "NO_SUCH_USER":
                dict_no_such_users=update_user_event_count(line, dict_no_such_users, keyword)
            elif keyword == "ACCOUNT_LOCKED_OUT":
                dict_locked_user=update_user_event_count(line, dict_no_such_users, keyword)
            else:
                dict_smb_events=update_smb_event_count(line, dict_smb_events, keyword)

        line_ctr = line_ctr + 1

        if (line_ctr % 100000 == 0):
            print("processed - " + str(line_ctr) + " lines")

    print("status: processed lines =" + str(line_ctr))
    print("")
    print_events_by_keyword(dict_smb_events, keywords)
    pretty_print(dict_smb_events)
    print("\n")
    print("=== Top 5 Users with WRONG_PASSWORD ===")
    print_top_n(dict_wrong_password,5)
    print("\n")
    print("==== Top 5 Users with NO_SUCH_USER ====")
    print_top_n(dict_no_such_users,5)
    print("\n")
    print("==== Top 5 Users with ACCOUNT_LOCKED_OUT ====")
    print_top_n(dict_locked_user,5)
    print("\n")
    print("==== List of Access Denies Per Day ====")
    #for item in sorted(list_access_denied):
    #    print("User = {}, IP = {}, Share = {}, Count= {}".format(item.user,
    #                                                           item.ip, item.share, item.count))
    print(len(list_access_denied))

if __name__ == '__main__':
    main()