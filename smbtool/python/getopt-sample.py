#
# getopt sample to test out parameters
#

import os
from os import path
import sys
import getopt
def usage():
    print("-h / --help this usage")
    print("-o / --output= file to write")
    print("-v verbose - really?")

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hoi:v", ["help", "output=","infile="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print
        str(err)  # will print something like "option -a not recognized"
        print("we hit some error")
        sys.exit(2)
    output = None
    verbose = False
    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-o", "--output"):
            output = a
        elif o in ("-i", "--infile"):
            infile = path.realpath(a)
        else:
            assert False, "unhandled option"

    print("output=" + str(output))
    print("verbose=" + str(verbose))
    print("infile =" + infile)

## actually this is where we execuate
if ( __name__ == "__main__"):
    main()

# end of code