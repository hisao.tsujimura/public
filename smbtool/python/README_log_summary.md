# README.md

This file explains how log_summary.py works.

## Goal

The goal of this script is to summarize a log file specified and does the followings.

1. The first event date/time and its message
1. The last event date/time and its message
1. The event type and its count
1. The raw event log for the first event
1. Count of events based on the "Search" keywords
1. Count the number of items specified by "Count" keywords. (*1)

(*1) More details in the later part of the document.

## To Do

The below are the list of items to be developed.

1. More keywords for different log types.
1. Reading configuration file to set up keywords.

## "Search" Keywords

The below is the sample key word specification for SMB logs.

```python
search_keywords = [
            'NO_SUCH_USER', 'WRONG_PASSWORD', 'ACCOUNT_EXPIRED',
            'smb_kdoor_chkhdr', 'smb_kdoor_upcall',
            'ndr_rpc_bind', 'smbd_opipe_exec', 'Resource temporarily unavailable',
            'domain service not responding', 'SID-to-name', 'SMBIOC_T2RQ failed',
            'smb_smb_treeconnect', 'Connection to Active Directory server failed'
            ]

# Note: You can specify the keywords in form of a "list."
```

Since this version of code support both SMB and NFS related messages, we specify them in different variables and switch between them.

## "Count" Keywords

The items that need to be counted can be specified by keyword and field number in the form of a list.
The fields here mean words separated by a space in single line, whose number start from 0.  
For example, if a line should have a keyword of "NO_SUCH_USER" and 6th column is the user name that you want to count, the below is how you specify it.

```python
count_keywords = [['NO_SUCH_USER',5]]
```

In the same manner, if 6th column for NO_SUCH_USER and 7th column for WRONG_PASSWORD, then you would specify below.

```python
count_keywords = [
                    ['NO_SUCH_USER',    5 ],
                    ['WRONG_PASSOWRD',  6 ]
                ]
```

## Syntax

Given the log file name as "infile," the below is the syntax.

```shell
python log_summary.py [--infile <infile>] [--type smb|nfs] \
        [--every <line count>] [--top N]
        [-t <start date/time> ] [ -T <end date/time>]
        [--start_time <start date/time> ]
        [--end_time <end date/time> ]
```

If you do not specify \<infile\>, then it would default to the file name specified in DEFAULT_INFILE in the script's global variable.
If --type is not specified, it will consider the "infile" to be an SMB log file.
If --every is specified, it will display the progress of log file every \<line count\> lines.
If --top is specified, counted items are sorted and it will display top N items.

## Time Filtering Support

By specifiying the start time and end time, it will filter out the events 
between the range.  The below is the behavior.

| Start Date/Time | End Date/Time | Result                                    |
|-----------------|---------------|-------------------------------------------|
| Not specified   | Not specified | All logs are looked at.                   |
| Specified       | Not specified | From the time specified to the end.       |
| Not specified   | Specified     | From the beginning to the time specified. |

## Revision History

Last updated: 2018-02-22

(END OF DOCUMENT)

