
import unittest

# --------------------------------------------------------------
#  Test Scenarios
# --------------------------------------------------------------

class test_get_value_for_neg(unittest.TestCase):
    """
        Test get_value_for function.
        value should NOT be found.
    """

    def test_get_value_for_neg(self):
        print("get_value_for (neg)")
        d = dict()
        value = "Some value"
        expected = None
        actual = get_value_for(d, value)
        self.assertEqual(expected, actual)


class test_get_value_for_pos(unittest.TestCase):
    """
        Test get_value_for function.
        Value should the 'a value."
    """

    def test_get_value_for_pos(self):
        print("get_value_for (pos)")
        d = dict()
        key = "key1"
        value = "a value"
        d[key] = value
        expected = value
        actual = get_value_for(d, key)
        self.assertEqual(expected, actual)


class test_add_key_value_pos1(unittest.TestCase):
    """
        Test add_key_value function.
    """

    def test_get_value_pos(self):
        print("add_key_value (pos)")
        d = dict()
        key = "key1"
        value = "a value"
        expected = {'key1': 'a value'}
        actual = add_key_value(d, key, value)
        self.assertEqual(expected, actual)

    def test_get_value_neg(self):
        print("add_key_value (neg)")
        d = dict()
        key = "key1"
        value = "a value"
        d = add_key_value(d, key, value)
        key = "key2"
        value = 2
        d = add_key_value(d, key, value)
        expected = {'key2': 2}
        actual = d[key]
        self.assertNotEqual(expected, actual)


class test_get_nth_field(unittest.TestCase):
    """
        test get_nth_field.
        Given a sample line, check results.
    """

    def test_get_nth_field_pos_space(self):
        test_string = "Fields: 1 2 3 4 5"

        print("get_nth_filed - field 0 (space)")
        actual = get_nth_field(test_string, 0)
        expected = "Fields:"
        self.assertEqual(expected, actual)

        print("get_nth_filed - field 2 (space)")
        actual = get_nth_field(test_string, 2)
        expected = "2"
        self.assertEqual(expected, actual)

    def test_get_nth_field_neg_space(self):
        test_string = "Fields: 1 2 3 4 5"

        print("get_nth_filed - field 7 (space) > max")

        actual = get_nth_field(test_string, 7)
        expected = "N/A"
        self.assertEqual(expected, actual)

    def test_get_nth_field_pos_colon(self):
        test_string = "Fields:1:2:3:4:5"

        print("get_nth_filed - field 0 (colon)")
        actual = get_nth_field(test_string, 0, ":")
        expected = "Fields"
        self.assertEqual(expected, actual)

        print("get_nth_filed - field 2 (colon)")
        actual = get_nth_field(test_string, 2, ":")
        expected = "2"
        self.assertEqual(expected, actual)


class test_get_time_stamp(unittest.TestCase):
    """
    Test time stamp related functions
    """

    def test_get_timestamp_pos1_fmt1(self):
        line = "2017/12/01 13:45 It's a birthday."

        print("get_time_stamp - fmt1, pos1")

        actual = get_time_stamp(line)
        expected = "2017/12/01"
        self.assertEqual(expected, actual)

    def test_get_timestamp_pos2_fmt1(self):
        line = "2017/12/01 13:45 It's a birthday."

        print("get_time_stamp - fmt1, pos2")

        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = "2017/12/01"
        self.assertEqual(expected, actual)

    def test_get_timestamp_neg1_fmt1(self):
        line = None
        print("get_time_stamp - fmt1, neg1")
        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = None
        self.assertEqual(expected, actual)

    def test_get_timestamp_neg2_fmt1(self):
        line = "12345"
        print("get_time_stamp - fmt1, neg2")

        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = None
        self.assertEqual(expected, actual)


class test_dict_empty(unittest.TestCase):

    def test_dict_empty_pos1(self):
        print("dict_empty - pos1")
        d = dict()
        expected = True
        actual = dict_empty(d)
        self.assertEqual(expected, actual)

    def test_dict_empty_neg1(self):
        print("dict_empty - neg1")
        d = dict()
        d[0] = "Test"
        expected = False
        actual = dict_empty(d)
        self.assertEqual(expected, actual)

class test_event_add(unittest.TestCase):

    """
    test event_add function
    """

    def test_event_add_pos1(self):
        print("event_add - pos1")
        d = dict()
        line="event1 test"
        keyword="event1"

        expected={"event1_count":1}
        actual = event_add(line, d, keyword)

        self.assertEqual(expected, actual)

        print("event_add - pos2")
        d = dict()
        keyword="event1"
        line="event1 test"
        d = event_add(line, d, keyword)
        line="event1 test2"
        actual = event_add(line, d, keyword)
        expected={"event1_count":2}
        self.assertEqual(expected, actual)

    def test_event_add_neg1(self):
        print("event_add - neg1")
        d = dict()
        line="event1 test"
        keyword="nono"
        expected={}
        actual=event_add(line, d, keyword)
        self.assertEqual(expected, actual)

class test_set_first_event(unittest.TestCase):
    """
    test set_first_event function
    """

    def test_first_event_pos1(self):
        print("set_first_event - pos1")

        d=dict()
        keyword = "event"
        line = "event 1"

        expected = {"event_first":"event 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_pos2(self):
        print("set_first_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_first_event(line, d, keyword)
        line = "event 2"

        expected = {"event_first":"event 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_neg1(self):
        print("set_first_event - neg1")

        d = dict()
        keyword = "test"
        line = "event 1"
        expected ={}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_neg2(self):
        print("set_first_event - neg2")

        d = dict()
        keyword = "test"
        line = "event 1"
        d = set_first_event(line, d, keyword)
        line = "test 1"
        expected ={"test_first":"test 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

class test_set_last_event(unittest.TestCase):

    def test_set_last_event_pos1(self):
        print("set_last_event - pos1")

        d = dict()
        keyword = "event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_set_last_event_pos2(self):
        print("set_last_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_last_event(line, d, keyword)
        line = "event 2"
        expected ={"event_first":"event 1", "event_last":"event 2"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_set_last_event_neg1(self):
        print("set_last_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_last_event(line, d, keyword)
        line = "not 2"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

class test_record_events(unittest.TestCase):

    def test_record_events_pos1(self):
        print("test record_events (pos1)")

        d = dict()

        keyword="event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        line = "event 2"
        expected ={"event_first":"event 1", "event_last":"event 2"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_record_events_mix1(self):
        print("test record_events (mix 1)")

        d = dict()

        keyword="event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        keyword="test"
        line = "test 1"
        expected ={
            "event_first":"event 1", "event_last":"event 1",
            "test_first":"test 1", "test_last":"test 1"
        }
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        keyword="event"
        line = "event 2"
        expected ={
            "event_first": "event 1", "event_last": "event 2",
            "test_first": "test 1", "test_last": "test 1"
        }
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

def main():

    if __name__ == "__main__":
        unittest.main()
