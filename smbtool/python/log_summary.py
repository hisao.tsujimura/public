#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
# --------------------------------------------------------------
#
# SMB Tools
#
# Script name: log_summary.py
#
# How To Run:
#   log_summary.py [-i|--i <filename for analysis>] ¥
#       [--type [smb|nfs]
#
# What This Does:
#   Please see README_log_summary.md.
#
#  This program does not filter message by time stamp yet.
#
# * To Do:
#   Add filter-by-time feature
#
# * Revision
#   2017-11-14 2.0 Hisao Tsujimura
#
# --------------------------------------------------------------

# import argparse
import os, sys
from os import path
import unittest

# --------------------------------------------------------------
#  Exception Variables
# --------------------------------------------------------------

E_DICT_EMPTY = "Dictionary cannot be empty."
E_DICT_DUPKEY = "You cannot specify a duplicate key."

# --------------------------------------------------------------
#  Date Formats (Not used to determine the date format yet)
# --------------------------------------------------------------
# TODO: Implement handling of different data formats.

FMT_YYYYMMDD = "yyyy/mm/dd"


# --------------------------------------------------------------
#  Exception Handler
# --------------------------------------------------------------

def handle_exception(func_name, message):
    raise Exception(func_name + ': ' + message)

# --------------------------------------------------------------
#  Dictionary Functions
# --------------------------------------------------------------

# Dictionary operations

#
# Check if Dictionary is empty
#


def dict_empty(d):
    if len(d) == 0:
        return (True)
    else:
        return (False)


#
# Get a value against a key
#

def get_value_for(d, key):
    if len(d) == 0:
        return (None)

    if key in d:
        return (d[key])
    else:
        return (None)


#
# Add name, value pair into the specified dictionary
# It returns an updated dictionary
#

def add_key_value(d, key, value):
    func_name = "add_key_value()"

    # If the dictionary size is zero (empty) add the key-value
    # without questioning.

    if len(d) == 0:
        d[key] = value
        return (d)

    # Check if the key already exist in the dictionary.
    # If there is, the caller needs to update instead of adding value,
    # so thrown an exception.

    if key in d:
        handle_exception(func_name, E_DICT_DUPKEY)

    # We add key-value when duplicate key is not found.
    d[key] = value

    return (d)


# ----------------------------------------------------------------------------
# Processing A Line
# ----------------------------------------------------------------------------

# get a field divided by the delimiter
# we assume space if not specified, otherwise use whatever is passed.

def get_nth_field(line, n, delimiter=" "):

    a_white_space = " "
    if delimiter is not a_white_space:
        tokens = line.split(delimiter)
    else:
        tokens = line.split()

    max_field_no = len(tokens)

    if n > max_field_no:
        return ("N/A")

    curved_field = tokens[n]

    return (curved_field)


#
# Get time stamp
#   Get various time stamp from the supplied line.
#   We are supporting more formats, but right now,
#   we are pretty dumb, just chopping off the string.
#

# actual code to manage time stamp

def get_time_stamp_impl(line, date_type=FMT_YYYYMMDD):
    if line is None:
        return (None)

    if len(line) <= len(date_type):
        return (None)

    if date_type is None:
        date_type = FMT_YYYYMMDD

    # we only accept one format for now.
    # TODO: implement more format

    if date_type != FMT_YYYYMMDD:
        return (None)

    date_len = len(date_type)

    return (line[0:date_len])


# wrapper

def get_time_stamp(line, date_type=FMT_YYYYMMDD):
    time_in_string = get_time_stamp_impl(line, date_type)

    return (time_in_string)


# ----------------------------------------------------------------------------
# Count Events
# ----------------------------------------------------------------------------

# counter related functions

def event_add(line, d, keyword):

    if keyword in line:

        key = keyword + "_count"

        if key in d:
            d[key] = d[key] + 1
        else:
            d[key] = 1

    return (d)


# --------------------------------------------------------------
#  Record / Read First/Last Events
# --------------------------------------------------------------

# Given the line, dictionary name and keyword, record the
# first event.  If thee key is not previously found,
# it's th first event.

def set_first_event(line, d, keyword):

    # if keyword does not match, just return d
    if keyword not in line:
        return(d)

    key = keyword + "_first"

    if get_value_for(d, key) is None:
        d[key] = line

    return (d)


# Given the line, keep record of the last event.
# If keyword is found, keep updating.  After we are done
# going through all the lines, it's the last record.

def set_last_event(line, d, keyword):

    # if keyword does not match, just return d
    if keyword not in line:
        return(d)

    key = keyword + "_last"

    # it should not happen, but if this is called
    # before the first event is found, set it.
    if get_value_for(d, key) is None:
        d = set_first_event(line, d, keyword)

    d[key] = line

    return (d)


#
#  a wrapper for keeping record of events.
#

def record_events(line, d, keyword):
    d = set_first_event(line, d, keyword)
    d = set_last_event(line, d, keyword)

    return (d)


#
# Get the first event
#

def get_first_event(d, keyword):
    key = keyword + "_first"

    if get_value_for(d, key) is None:
        return (None)
    else:
        return (d[key])
#
# Get the last event
#

def get_first_last(d, keyword):
    key = keyword + "_last"

    if get_value_for(d, key) is None:
        return (None)
    else:
        return (d[key])

# --------------------------------------------------------------
#  Test Scenarios
# --------------------------------------------------------------

class test_get_value_for_neg(unittest.TestCase):
    """
        Test get_value_for function.
        value should NOT be found.
    """

    def test_get_value_for_neg(self):
        print("get_value_for (neg)")
        d = dict()
        value = "Some value"
        expected = None
        actual = get_value_for(d, value)
        self.assertEqual(expected, actual)


class test_get_value_for_pos(unittest.TestCase):
    """
        Test get_value_for function.
        Value should the 'a value."
    """

    def test_get_value_for_pos(self):
        print("get_value_for (pos)")
        d = dict()
        key = "key1"
        value = "a value"
        d[key] = value
        expected = value
        actual = get_value_for(d, key)
        self.assertEqual(expected, actual)


class test_add_key_value_pos1(unittest.TestCase):
    """
        Test add_key_value function.
    """

    def test_get_value_pos(self):
        print("add_key_value (pos)")
        d = dict()
        key = "key1"
        value = "a value"
        expected = {'key1': 'a value'}
        actual = add_key_value(d, key, value)
        self.assertEqual(expected, actual)

    def test_get_value_neg(self):
        print("add_key_value (neg)")
        d = dict()
        key = "key1"
        value = "a value"
        d = add_key_value(d, key, value)
        key = "key2"
        value = 2
        d = add_key_value(d, key, value)
        expected = {'key2': 2}
        actual = d[key]
        self.assertNotEqual(expected, actual)


class test_get_nth_field(unittest.TestCase):
    """
        test get_nth_field.
        Given a sample line, check results.
    """

    def test_get_nth_field_pos_space(self):
        test_string = "Fields: 1 2 3 4 5"

        print("get_nth_filed - field 0 (space)")
        actual = get_nth_field(test_string, 0)
        expected = "Fields:"
        self.assertEqual(expected, actual)

        print("get_nth_filed - field 2 (space)")
        actual = get_nth_field(test_string, 2)
        expected = "2"
        self.assertEqual(expected, actual)

    def test_get_nth_field_neg_space(self):
        test_string = "Fields: 1 2 3 4 5"

        print("get_nth_filed - field 7 (space) > max")

        actual = get_nth_field(test_string, 7)
        expected = "N/A"
        self.assertEqual(expected, actual)

    def test_get_nth_field_pos_colon(self):
        test_string = "Fields:1:2:3:4:5"

        print("get_nth_filed - field 0 (colon)")
        actual = get_nth_field(test_string, 0, ":")
        expected = "Fields"
        self.assertEqual(expected, actual)

        print("get_nth_filed - field 2 (colon)")
        actual = get_nth_field(test_string, 2, ":")
        expected = "2"
        self.assertEqual(expected, actual)


class test_get_time_stamp(unittest.TestCase):
    """
    Test time stamp related functions
    """

    def test_get_timestamp_pos1_fmt1(self):
        line = "2017/12/01 13:45 It's a birthday."

        print("get_time_stamp - fmt1, pos1")

        actual = get_time_stamp(line)
        expected = "2017/12/01"
        self.assertEqual(expected, actual)

    def test_get_timestamp_pos2_fmt1(self):
        line = "2017/12/01 13:45 It's a birthday."

        print("get_time_stamp - fmt1, pos2")

        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = "2017/12/01"
        self.assertEqual(expected, actual)

    def test_get_timestamp_neg1_fmt1(self):
        line = None
        print("get_time_stamp - fmt1, neg1")
        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = None
        self.assertEqual(expected, actual)

    def test_get_timestamp_neg2_fmt1(self):
        line = "12345"
        print("get_time_stamp - fmt1, neg2")

        actual = get_time_stamp(line, FMT_YYYYMMDD)
        expected = None
        self.assertEqual(expected, actual)


class test_dict_empty(unittest.TestCase):

    def test_dict_empty_pos1(self):
        print("dict_empty - pos1")
        d = dict()
        expected = True
        actual = dict_empty(d)
        self.assertEqual(expected, actual)

    def test_dict_empty_neg1(self):
        print("dict_empty - neg1")
        d = dict()
        d[0] = "Test"
        expected = False
        actual = dict_empty(d)
        self.assertEqual(expected, actual)

class test_event_add(unittest.TestCase):

    """
    test event_add function
    """

    def test_event_add_pos1(self):
        print("event_add - pos1")
        d = dict()
        line="event1 test"
        keyword="event1"

        expected={"event1_count":1}
        actual = event_add(line, d, keyword)

        self.assertEqual(expected, actual)

        print("event_add - pos2")
        d = dict()
        keyword="event1"
        line="event1 test"
        d = event_add(line, d, keyword)
        line="event1 test2"
        actual = event_add(line, d, keyword)
        expected={"event1_count":2}
        self.assertEqual(expected, actual)

    def test_event_add_neg1(self):
        print("event_add - neg1")
        d = dict()
        line="event1 test"
        keyword="nono"
        expected={}
        actual=event_add(line, d, keyword)
        self.assertEqual(expected, actual)

class test_set_first_event(unittest.TestCase):
    """
    test set_first_event function
    """

    def test_first_event_pos1(self):
        print("set_first_event - pos1")

        d=dict()
        keyword = "event"
        line = "event 1"

        expected = {"event_first":"event 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_pos2(self):
        print("set_first_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_first_event(line, d, keyword)
        line = "event 2"

        expected = {"event_first":"event 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_neg1(self):
        print("set_first_event - neg1")

        d = dict()
        keyword = "test"
        line = "event 1"
        expected ={}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_first_event_neg2(self):
        print("set_first_event - neg2")

        d = dict()
        keyword = "test"
        line = "event 1"
        d = set_first_event(line, d, keyword)
        line = "test 1"
        expected ={"test_first":"test 1"}
        actual = set_first_event(line, d, keyword)
        self.assertEqual(expected, actual)

class test_set_last_event(unittest.TestCase):

    def test_set_last_event_pos1(self):
        print("set_last_event - pos1")

        d = dict()
        keyword = "event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_set_last_event_pos2(self):
        print("set_last_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_last_event(line, d, keyword)
        line = "event 2"
        expected ={"event_first":"event 1", "event_last":"event 2"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_set_last_event_neg1(self):
        print("set_last_event - pos2")

        d = dict()
        keyword = "event"
        line = "event 1"
        d = set_last_event(line, d, keyword)
        line = "not 2"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual = set_last_event(line, d, keyword)
        self.assertEqual(expected, actual)

class test_record_events(unittest.TestCase):

    def test_record_events_pos1(self):
        print("test record_events (pos1)")

        d = dict()

        keyword="event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        line = "event 2"
        expected ={"event_first":"event 1", "event_last":"event 2"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

    def test_record_events_mix1(self):
        print("test record_events (mix 1)")

        d = dict()

        keyword="event"
        line = "event 1"
        expected ={"event_first":"event 1", "event_last":"event 1"}
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        keyword="test"
        line = "test 1"
        expected ={
            "event_first":"event 1", "event_last":"event 1",
            "test_first":"test 1", "test_last":"test 1"
        }
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

        keyword="event"
        line = "event 2"
        expected ={
            "event_first": "event 1", "event_last": "event 2",
            "test_first": "test 1", "test_last": "test 1"
        }
        actual=record_events(line, d, keyword)
        self.assertEqual(expected, actual)

# -------------------------------------------------------------
#  Main
# --------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()
