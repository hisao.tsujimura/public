#!/usr/bin/python -tt
# -*- coding: utf-8 -*-
# --------------------------------------------------------------
#
# SMB Tools
#
# Script name: smb_uerrors.py
#
# How To Run:
#   smb_uerrors.py <file name for analysis>
#
# What This Does
#
#   It counts the number of events in the given file against
#   the below events.
#
#   NO_SUCH_USER    -> User does not exist
#   WRONG_PASSWORD  -> The wrong password was supplied.
#
#   It also counts the event by user name for above events.
#
#   In addition, the below events are counted.
#
# TO DO
#   Add feature to take argument for the file name
#
# * Revision
#   2017-06-19  0.2 Hisao Tsujimura
#
# --------------------------------------------------------------

"""
Sample SMB Log Parser

This Python script uses 1 list for each event to store the user names
with errors.  They are as in below.

    ns_userList - NO_SUCH_USER
    wp_userList - WRONG_PASSWORD

We use another set of list to count number of events.
Each item look something like below in the list

    username, count

    ns_errorList - NO_SUCH_USER
    wp_errorList - WRONG_PASSWORD


"""

# import libraries

import os
from os import path
import sys
import getopt

#-------------------------------------------------------------------------
# functions
#-------------------------------------------------------------------------

# check if the NO_SUCH_USER keyword is found in the line
# return true, if it is found.
# return false, if not found.

def isNoSuchUser(line):
    pos=0

    pos=line.find('NO_SUCH_USER')

    if pos < 0:
        return False
    else:
        return True

# check if the WRONG_PASSWORD keyword is found in the line
# return true, if it is found.
# return false, if not found.

def isWrongPassword(line):
        pos = 0

        pos = line.find('WRONG_PASSWORD')

        if pos < 0:
            return False
        else:
            return True

# get user name
# get Nth token and clean it up as user name.
# If ZFSSA debug.sys or Solaris smbd message N=8.

def getUserName(line, n):
    tokens=line.split(' ')
    userinfo=str(tokens[n])

    posLeftBracket=userinfo.find('[')
    posRightBracket=userinfo.find(']')

    username=str(userinfo[posLeftBracket+1:posRightBracket-1])

    return(username)

#---------
# User List Management
#---------

#
# check if the user name exist in the list.
# True if exist, False if not

def findUserFromList(username,userList):
    return(username in userList)

def getUserPosFromList(username,userList):
    return(userList.index(username))

#
# Add to user list
#   Add user to the list and return whole list back
#
def addUserIntoList(debug, username,userList):
    if findUserFromList(username, userList):
        if debug:
            print('// user list stays same')
    else:
        if debug:
            print('// user list grows')
        userList.append(username)

    return(userList)

#---------
# Event List Management
#---------

#
# Add event to event list
#   Use user_index to find the event count
#  (i.e. user_index = index in the event list)
#   If list is empty, ignore user_index and add an entry.
#   If the list is NOT empty then
#       when match, update the counter
#       when not
#
def addEventIntoList(debug, user_index, eventList):
    # whenver the size of user index is larger than size of list,
    # append at the end

    sizeof_eventList=len(eventList)

    if debug:
        print('// addEventIntoList - user_index=' + str(user_index) +
              ', size=' + str(sizeof_eventList))

    if not eventList:
        if debug:
            print('// Event list was empty')
        eventList.append(1)
    else:
        if debug:
            print('// Event list is NOT empty')

        if user_index >= sizeof_eventList:
            if debug:
                print('//  (event list grows)')
            eventList.append(1)
        else:
            if user_index < sizeof_eventList:
                if debug:
                    print('// (+1)')
                eventList[user_index] = eventList[user_index] + 1

    dump_values(debug,eventList, 'event list')
    return(eventList)

#
# Dump Content of list
#
def dump_values(debug,listname,label):
    if debug:
        if listname:
            print('## DEBUG list name = ' + label + ', size of list=' + str(len(listname)))
            print(listname)
        else:
            print('## DEBUG list name = ' + label + ' is empty.')
    return

#---------
# Argument Functions
#---------

def get_args():

    def usage():
        print("-i / --infile input file name")
        print("-h / -help This help")

    default_infile=u'/Users/hisaotsu/Documents/050_開発/work/temp/logs/debug.sys'
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi", ["help", "infile"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print
        str(err)  # will print something like "option -a not recognized"
        print("we hit some error")
        sys.exit(2)

    infile = default_infile

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-i", "--infile"):
            infile = path.realpath(a)
        else:
            print("using default infile")
            assert False, "Unhandled option"

    return(infile)

#-------------------------------------------------------------------------
# Main
#-------------------------------------------------------------------------

def main():

    # debug flag

    F_DEBUG=False

    # input file name
    filename = get_args()
    # open the file read-only

    # check for file existence is necessary.
    fin = open(filename)
    line_count=0

    # for NO_SUCH_USERS
    ns_userList=[]
    ns_userList.clear()
    ns_eventList = []
    ns_eventList.clear()

    # for WRONG_PASSWROD
    wp_userList=[]
    wp_userList.clear()
    wp_eventList = []
    wp_eventList.clear()

    # simply read the file and print them out.

    while True:
        line = fin.readline()
        if not line:
            break

        line_count=line_count+1
        pos_user=0

        # if NO_SUCH_USER is found
        if isNoSuchUser(line):
            # get user, check if it is in the list.
            # since Solaris, N=8 to find the user name

            username=getUserName(line,8)

            # If the user is NOT found, put user into the list and add an event.

            if not findUserFromList(username, ns_userList):
                ns_userList=addUserIntoList(False, username, ns_userList)
                ns_eventList=addEventIntoList(False, len(ns_eventList), ns_eventList)

            # if found, just add an event
            else:
                pos_user=getUserPosFromList(username,ns_userList)
                ns_eventList=addEventIntoList(False, pos_user, ns_eventList)

            if F_DEBUG:
                print(' ns_userlist (size) = ' + str(sizeof_ns_userList))
                print(' ns_eventlist (size) = ' + str(sizeof_ns_eventList))
                dump_values(F_DEBUG, ns_userList, 'ns_userList')
                dump_values(F_DEBUG, ns_eventList, 'ns_eventList')

        # if WRONG_PASSWORD is found

        if isWrongPassword(line):
            # get user, check if it is in the list.
            # since Solaris, N=8 to find the user name

            username=getUserName(line,8)

            # If the user is NOT found, put user into the list and add an event.

            if not findUserFromList(username, wp_userList):
                wp_userList=addUserIntoList(False, username, wp_userList)
                wp_eventList=addEventIntoList(False, len(wp_eventList), wp_eventList)

            # if found, just add an event
            else:
                pos_user=getUserPosFromList(username,wp_userList)
                wp_eventList=addEventIntoList(False, pos_user, wp_eventList)

            if F_DEBUG:
                print(' wp_userlist (size) = ' + str(sizeof_wp_userList))
                print(' wp_eventlist (size) = ' + str(sizeof_wp_eventList))
                dump_values(F_DEBUG, wp_userList, 'wp_userList')
                dump_values(F_DEBUG, wp_eventList, 'wp_eventList')

    # now file is read, let's print out NO_SUCH_USER events

    i=0
    sizeof_ns_userList = len(ns_userList)
    sizeof_ns_eventList = len(ns_eventList)

    print('======= NO SUCH PASSWORD ======')
    while i < sizeof_ns_userList:
        eventCount=ns_eventList[i]
        username=ns_userList[i]

        print('i= ' + str(i) + ', ' +
              'User= ' + username +
              ' (count= ' + str(eventCount) + ')')
        i = i + 1

    # now file is read, let's print out WRONG_PASSWORD events

    i = 0
    sizeof_wp_userList = len(wp_userList)
    sizeof_wp_eventList = len(wp_eventList)

    print('======= WRONG PASSWORD ======')
    while i < sizeof_wp_userList:
        eventCount = wp_eventList[i]
        username = wp_userList[i]

        print('i= ' + str(i) + ', ' +
              'User= ' + username +
              ' (count= ' + str(eventCount) + ')')
        i = i + 1

if __name__ == '__main__':
    main()

## end of script
