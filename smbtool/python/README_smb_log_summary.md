# README.md

1. Script Name

smb_log_summary.py

2. What This Does

This script reads /var/ak/logs/debug.sys and summarizes the events based on 
the predefined keywords in the script. It will give you the following
information.

    (a) The date/time of the first event
    (b) The date/time of the latest event
    (c) A full event message for the first event
    
For following two keywords, it will provide top 5 names.

    WRONG_PASSWORD
    NO_SUCH_USER

The output will be filtered by the start and end time specified by the
user.  Please refer to "Time Filtering Support" section.

3. Usage

usage: smb_log_summary.py [-h] [-i INFILE] [--infile INFILE] [-t START_TIME]
                          [--start-time START_TIME] [-T END_TIME]
                          [--end-time END_TIME]

optional arguments:
  -h, --help            show this help message and exit
  -i INFILE             path to the log file
  --infile INFILE       path to the log file
  -t START_TIME         Specify the date/time format in either YYYY-MM-DD or
                        YYYY-MM-DD HH:mm:ss
  --start-time START_TIME
                        Specify the date/time format in either YYYY-MM-DD or
                        YYYY-MM-DD HH:mm:ss
  -T END_TIME           Specify the date/time format in either YYYY-MM-DD or
                        YYYY-MM-DD HH:mm:ss
  --end-time END_TIME   Specify the date/time format in either YYYY-MM-DD or
                        YYYY-MM-DD HH:mm:ss
4. Prerequisites

This program assumes running Python3 and "argparse" package.

5. Predefined Hardcoded Keywords

The below are predefined hard coded keywords.
The keywords are provided in Python "list."  Each element of the list must be
a string.

    keywords = [ 'NO_SUCH_USER', 'WRONG_PASSWORD',
                 'smb_kdoor_chkhdr', 'smb_kdoor_upcall',
                 'ndr_rpc_bind', 'smbd_opipe_exec', 'Resource temporarily unavailable',
                 'domain service not responding', 'SID-to-name', 'SMBIOC_T2RQ failed',
                 'smb_smb_treeconnect', 'Connection to Active Directory server failed']

6. Time Filtering Support

By specifiying the start time and end time, it will filter out the events 
between the range.  The below is the behavior.

| Start Date/Time | End Date/Time | Result                                    |
|-----------------|---------------|-------------------------------------------|
| Not specified   | Not specified | All logs are looked at.                   |
| Specified       | Not specified | From the time specified to the end.       |
| Not specified   | Specified     | From the beginning to the time specified. |

7. TODO

The following features are work in progress.

(a) Keyword configuraiton file support

    Currently, the keywords are fixed.  Keyword configuration file will
    provide users the flexibility to customize the keywords to focus on.

(b) More date format support
    Currently we only support YYYY-MM-DD HH:mm:ss.
    While other format should work if Python supports it, however, it is not tested.

(c) Time-shift support
    Log files are from diferent time zones.  While the current code assume no timezone,
    shifting hours to match timezones would be helpful.

7. Issues 

For any issues, please use issue tracker on the repository.
Also, you can reach me at the following email addresss.

    hisao.tsujimura@gmail.com 

(END OF DOCUMENT)
