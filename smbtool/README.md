# README.md

Created: 2017-01-25 by Hisao Tsujimura
Last Updated: 2017-03-05

## 0. Disclaimer

The scripts are still under development.

## 1. Introduction

There are times with either Solaris or ZFS Storage Appliance, we need to quickly process large quantitiy of logs.  smbtools directory contains small tools that I use at work for "skimming" through the logs.  The collection of the scripts is to achieve such goal.

## 2. The Scope of tools

(1) Summary of User Connectivity Errors such as in below.
    - WRONG_PASSWORD
    - NO_SUCH_USER
(2) Summary of key authentication related problems such as SID lookup failures
(3) Summary of domain controller switch
(4) Basic configuraiton recommendations
(5) Dumping smb related information from smb core (Solaris only)

## 3. Out of Scope

While the some of the below are essential for troubleshooting ActiveDirectory environment, because the supportbundle or Oracle Explorer misses information for analysis, the below are out of scope.

(1) List of available domain controllers
(2) List of avaiable domain controllers in a specific "site."
(3) Answers to DNS queries in below document [^1] and if DNS servers agree.

[^1]: [How DNS Support for Active Directory Works](https://technet.microsoft.com/en-us/library/cc759550(v=WS.10).aspx)

## 4. Tested Platforms

The below are tested platforms.

- ZFS Storage Appliance Software 2013.1.6.8.
- Oracle Solaris 11.3 
- MacOS 10.12 (except script that use mdb)

Regardless of tested platforms, the most script works except the one that has extensive use of Solaris modular debugger (mdb) on flavors of Linux and MacOS.

## 5. How To Install

Download the script to your directory of your choice and give read and execute permission.

## 6. Role of Each script

The below are the name of script and their roles on the date of that this document is created.

| Name of Script  | Summar of Role                    |
|-----------------|-----------------------------------|
| smb_summary.sh  | Main script to run other scripts. |
| smb_uerrors.sh  | Find user related errors in log.  |
| smb_autherr.sh  | Find authentication errors.       |
| smb_consult.sh  | Suggest better configuraiton.     |
| smb_corelook.sh | Look into smb core file           |

## 7. TO-DO List

[X] Write a Python version of above scripts

## 8. How To Run

From the command line, execuate ./smb_summary.sh to generate all reports.
The indivisual scripts should work as well, however, you need to specify the file location.  The script names and referenced file names are listed below.
If you run smb_summar.sh, it assumes the current directory contains the supportbundle and passes the necessary paths to each scripts under the current directory.

| Name of Script  | Files Used                        |
|-----------------|-----------------------------------|
| smb_uerrors.sh  |                                   |
| smb_autherr.sh  |                                   |
| smb_consult.sh  |                                   |
| smb_corelook.sh |                                   |

## 9. Python Versions

Due to mainly the performance of the script, there is a Python version.
If the Python version has the difference from the shell version, that would be listed here.
Pyton scripts has .py extensions and assumes Python 3 for its environment.

### 9.1 Differences (smb_uerrors.sh and sub_uerrors.py)

smb_uerrors.py is complete, however, the below is the limitation as of now.

- Users cannot specify the file name as an argument, but have to hard code it.

## 10. Contact information

Hisao Tsujimura <hisao.tsujimura@gmail.com>

### 10.1 Filing Bugs or Enhancement Requests

N/A

### 10.2 Questions and Answers

N/A

## 10. Sample Reports

### smb_uerrors.py

```
======= NO SUCH PASSWORD ======
i= 0, User= AACORP\APXH860310 (count= 360)
i= 1, User= AACORP\APXH850180 (count= 135)
i= 2, User= AACORP\APXH850360 (count= 30)
i= 3, User= AACORP\APXH006278 (count= 518)
i= 4, User= AACORP\APXH861352 (count= 156)
i= 5, User= AACORP\APXH860793 (count= 798)
i= 6, User= AACORP\APXH005125 (count= 36)
(.....)
======= WRONG PASSWORD ======
i= 0, User= jxx.dns\ac641 (count= 20058)
i= 1, User= jxx\ac748 (count= 1)
i= 2, User= jxx\ac662 (count= 7)
i= 3, User= jxx\ac387 (count= 2)
i= 4, User= jxx\kc121 (count= 3)
i= 5, User= jxx\kc121 (count= 1)
(....)
```

(END OF README.md)

